import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceAdminOrganismComponent } from './finance-admin-organism.component';

describe('FinanceAdminOrganismComponent', () => {
  let component: FinanceAdminOrganismComponent;
  let fixture: ComponentFixture<FinanceAdminOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceAdminOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceAdminOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
