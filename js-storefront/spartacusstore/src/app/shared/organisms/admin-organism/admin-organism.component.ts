import { Component, OnInit } from '@angular/core';
import { SETUP_NAV_LINKS } from '../../../services/admin.service'

@Component({
  selector: 'app-admin-organism',
  templateUrl: './admin-organism.component.html',
  styleUrls: ['./admin-organism.component.scss']
})
export class AdminOrganismComponent implements OnInit {

  setupNavLinks: any
  panelOpenState = true;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png'
  navTab: string = SETUP_NAV_LINKS[0].name;

  constructor() { 
    this.setupNavLinks = SETUP_NAV_LINKS
  }

  ngOnInit() {
  }

  changeAccordian = () => {
   // this.contactInfo = true
  }

}
