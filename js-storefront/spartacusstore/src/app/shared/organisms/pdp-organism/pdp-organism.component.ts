import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';
import { PdpService } from '../../../services/pdp.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-pdp-organism',
  templateUrl: './pdp-organism.component.html',
  styleUrls: ['./pdp-organism.component.scss']
})
export class PdpOrganismComponent implements OnInit {

  hostName: string = environment.hostName;
  imageUrl: any;
  productName: any;
  price: any;
  description: any;
  productCode: any;
  isFinance: any = false;
  succMsg: any = 'Product successfully added to cart !';
  errMsg: any = 'Product not added. Please try again later !';
  maxQtyErr: any = false;
  maxQtyErrMsg: any = 'Please enter any value less than or equal to 50';
  usersQty: number;
  prodCode: any;
  showSubscription: boolean = false;
  isHrms: boolean = false;
  shouldShowAddToCart: boolean = true;
  range: any = "PRICERANGE1";
  _array = Array;
  constructor(private route: ActivatedRoute, private utilityService: CommonUtilityService, private router: Router,
              private pdpService: PdpService, private matSnack: MatSnackBar, private detect: ChangeDetectorRef) { }

  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.prodCode = (params['pid'].split(' ')[0]); //log the value of pid
      console.log("product code", this.prodCode);
    });
    const pdpUrl = environment.pdpEndPoint + this.prodCode;
    this.utilityService.getRequest(pdpUrl, '').subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      if (response) {
        this.imageUrl = response.picture.url;
        this.productName =  response.name;
        this.price = this.prodCode === "HRMS" ? response.hrmsPriceData.priceDataList : 'SAR ' + response.price.value.toFixed(2);
        this.description = response.description;
        this.productCode = response.code;
        if (this.productCode === 'Finance') {
          this.isFinance = true;
        }
        if(this.productCode === "HRMS") {
          this.isHrms = true;
          this.shouldShowAddToCart = false;
        }
      }
    });
}
handleShowSubscription() {
  this.showSubscription = !this.showSubscription;
  this.shouldShowAddToCart = !this.shouldShowAddToCart;
}
handleTerms() {
  this.shouldShowAddToCart = !this.shouldShowAddToCart;
}
handleChange(event) {
  this.maxQtyErr = false;
  this.usersQty = event.target.value ;
  this.usersQty > 50 ? ( this.maxQtyErr = true, this.usersQty = 50) :  null;
}
checkCart() {
  if (this.utilityService.checkLoggedInOrRedirect() && this.utilityService.getCookie('displayUid')) {
     // Check Cart Exists
     const userId = this.utilityService.getCookie('displayUid');
     const url = environment.hostName + environment.cartAPI + userId + '/carts';
     this.pdpService.checkCartExist(url).subscribe((data: any) => {
      const response = JSON.parse(JSON.stringify(data));
      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {
        this.addToCart(url, response.carts[0].code);
      } else {
           // Create new Cart
        this.pdpService.createCart(url).subscribe((res: any) => {
          const result = JSON.parse(JSON.stringify(res));
          if (result && result.code) {
            this.addToCart(url, result.code);
          }
        }, (err) => {
          this.matSnack.open(this.errMsg, 'close', {
            duration: 5000,   verticalPosition: 'top'
          });
         });
      }
     }, (err) => {
      this.matSnack.open(this.errMsg, 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
     });
  }
}

rangeSelect(e) {
  this.range = e.value;
}

addSubscription() {
  const userId = this.utilityService.getCookie('displayUid');
  const url = environment.hostName + environment.cartAPI + userId + '/pricerange';
  this.pdpService.subscription(url, this.range).subscribe((res: any) => {
    const result = JSON.parse(JSON.stringify(res));
    if (result.statusCode === 'success') {
      this.matSnack.open(this.succMsg, 'close', {
        duration: 5000,   verticalPosition: 'top'
      });
    }
  }, (err) => {
    this.matSnack.open(this.errMsg, 'close', {
      duration: 5000,   verticalPosition: 'top'
    });
   });
}

addToCart(url, cartId) {
  let qty;
  // Add product to the cart
  this.isHrms && this.addSubscription();
  url = url + '/' + cartId + '/entries';
  this.isFinance ?  qty = this.usersQty :  qty = 1;
  this.pdpService.addToCart( this.productCode, url, qty).subscribe((res: any) => {
    const result = JSON.parse(JSON.stringify(res));
    if (result.statusCode === 'success') {
      this.utilityService.setCookie('cartId', cartId);
      this.router.navigateByUrl('/cart');
    }
  }, (err) => {
    this.matSnack.open(this.errMsg, 'close', {
      duration: 5000,   verticalPosition: 'top'
    });
   });
}
}
