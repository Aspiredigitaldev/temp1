import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdpOrganismComponent } from './pdp-organism.component';

describe('PdpOrganismComponent', () => {
  let component: PdpOrganismComponent;
  let fixture: ComponentFixture<PdpOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdpOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdpOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
