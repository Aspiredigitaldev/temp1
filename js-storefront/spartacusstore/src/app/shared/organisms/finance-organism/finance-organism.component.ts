import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FINANCE_NAV_LINKS } from '../../../services/finance.service'

@Component({
  selector: 'app-finance-organism',
  templateUrl: './finance-organism.component.html',
  styleUrls: ['./finance-organism.component.scss']
})
export class FinanceOrganismComponent implements OnInit {

  financeNavLinks: any
  panelOpenState = true;
  hrImage: any = '../../assets/images/icn_masterdataicn_masterdata_blacksmall.png'
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder) { 
    this.financeNavLinks = FINANCE_NAV_LINKS
  }

  changeAccordian(){
    console.log("for futire use");
  }
  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }
}
