import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceOrganismComponent } from './finance-organism.component';

describe('FinanceOrganismComponent', () => {
  let component: FinanceOrganismComponent;
  let fixture: ComponentFixture<FinanceOrganismComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceOrganismComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceOrganismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
