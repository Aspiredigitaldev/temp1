import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminNavMobileMoleculeComponent } from './admin-nav-mobile-molecule.component';

describe('AdminNavMobileMoleculeComponent', () => {
  let component: AdminNavMobileMoleculeComponent;
  let fixture: ComponentFixture<AdminNavMobileMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminNavMobileMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminNavMobileMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
