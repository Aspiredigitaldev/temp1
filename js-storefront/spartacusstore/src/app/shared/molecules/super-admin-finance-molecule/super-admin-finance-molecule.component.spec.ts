import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminFinanceMoleculeComponent } from './super-admin-finance-molecule.component';

describe('SuperAdminFinanceMoleculeComponent', () => {
  let component: SuperAdminFinanceMoleculeComponent;
  let fixture: ComponentFixture<SuperAdminFinanceMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminFinanceMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminFinanceMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
