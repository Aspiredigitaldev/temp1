import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutUsMoleculeComponent } from './about-us-molecule.component';

describe('AboutUsMoleculeComponent', () => {
  let component: AboutUsMoleculeComponent;
  let fixture: ComponentFixture<AboutUsMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutUsMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutUsMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
