import { Component, Input } from '@angular/core';
import { CheckoutService, REVIEW_PATH, PAYMENT_PATH, MOBILE_DEVICE_SIZE } from '../../../services/checkout.service'
import { Router, ActivatedRoute  } from '@angular/router';

@Component({
  selector: 'cx-checkout-breadcrumb-molecule',
  templateUrl: './checkout-breadcrumb-molecule.component.html',
  styleUrls: ['./checkout-breadcrumb-molecule.component.scss'],
  host: {
    '(window:resize)': 'onResize($event)'
  }
})

export class CheckoutBreadcrumbMoleculeComponent {
  
  paymentContext: any
  checkoutBreadcrumb: any
  routePage: any
  activePage: any
  nextPage: any
  previousPage: any
  isMobileDevice: any = window.window.innerWidth >= 768 ? false : true

  constructor(
    private checkoutService: CheckoutService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) {
    
      this.checkoutBreadcrumb = this.checkoutService.setBreadcrumbToDefault()

    this.checkoutService.checkoutBreadcrumb.subscribe(context => {
      this.updateBreadcrumb(context)
      this.activePage = context

      if(this.activePage === REVIEW_PATH) {
        this.nextPage = null
        this.previousPage = PAYMENT_PATH
      } else if(this.activePage === PAYMENT_PATH) {
        this.nextPage = REVIEW_PATH
        this.previousPage = null
      }
    });
  }

  onResize(event){
    this.isMobileDevice = event.target.innerWidth >= MOBILE_DEVICE_SIZE ? false : true
  }

  updateBreadcrumb(page) {
    this.checkoutBreadcrumb = this.checkoutService.setBreadcrumbToDefault()

    this.checkoutBreadcrumb.forEach(activePage => {
      if(activePage.routePath === page) {
        activePage.isCurrentPage = true
      }
      if(page === REVIEW_PATH) {
        if(activePage.routePath === PAYMENT_PATH ) {
          activePage.isActive = true
        }
      }
    });
  }
  onChangePage(page) {
    if(page === REVIEW_PATH) {
      this.paymentContext = true
      this.checkoutService.changePaymentContext(this.paymentContext);
      if(this.checkoutService.getPaymentDetails !== null ) {
      this.checkoutBreadcrumb = page
      this.checkoutService.changeActivePage(this.checkoutBreadcrumb)
      this.router.navigate([page], {relativeTo: this.activatedRoute})
      }
    } else {
      this.checkoutBreadcrumb = page
      this.checkoutService.changeActivePage(this.checkoutBreadcrumb)
      this.router.navigate([page], {relativeTo: this.activatedRoute})
    }
  }
}
