import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceAdminHeaderMoleculeComponent } from './finance-admin-header-molecule.component';

describe('FinanceAdminHeaderMoleculeComponent', () => {
  let component: FinanceAdminHeaderMoleculeComponent;
  let fixture: ComponentFixture<FinanceAdminHeaderMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceAdminHeaderMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceAdminHeaderMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
