import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartCouponMoleculeComponent } from './cart-coupon-molecule.component';

describe('CartCouponMoleculeComponent', () => {
  let component: CartCouponMoleculeComponent;
  let fixture: ComponentFixture<CartCouponMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartCouponMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartCouponMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
