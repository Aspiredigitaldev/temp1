import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from "@angular/forms";
import { AdminService } from '../../../services/admin.service'
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';
import * as XLSX from 'xlsx';
import { json } from 'body-parser';

@Component({
  selector: 'app-admin-upload-molecule',
  templateUrl: './admin-upload-molecule.component.html',
  styleUrls: ['./admin-upload-molecule.component.scss']
})

export class AdminUploadMoleculeComponent implements OnInit {

  fileSelected: any = ''
  fileToUpload: any
  formGroup: FormGroup
  fileName: any


  constructor(
    private adminService: AdminService,
    private fb: FormBuilder,
    private matSnack: MatSnackBar
  ) {}

  ngOnInit() {
    this.formGroup = this.fb.group({
      file: [null, Validators.required]
    });
  }

  // fileInputChange(event: any) {
    
  //   this.fileSelected = event.target.files[0].name
  //   this.fileToUpload = event

  //   event = this.fileToUpload
  //   const reader = new FileReader();

  //   if (event.target.files && event.target.files.length) {
  //     this.fileName = event.target.files[0].name;
  //     const [file] = event.target.files;
  //     reader.readAsDataURL(file);

  //     reader.onload = () => {
  //       this.formGroup.get('file').setValue({
  //         filename: file.name,
  //         filetype: file.type,
  //         value: reader.result
  //       })
  //     };
  //   }
  // }

  fileInputChange(event: any) {
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = event.target.files[0];
    reader.onload = () => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      jsonData ? this.sendRequest(jsonData) : '';
    };
    reader.readAsBinaryString(file);
  }

  sendRequest(request) {
    this.adminService.submitForm(request).subscribe((res: any) => {
      if(res.active) {
        this.matSnack.open('Form submitted successfully', 'close', {
          duration: 5000,    verticalPosition: 'bottom'
        });
      } else {
        this.matSnack.open('Please try after sometime', 'close', {
          duration: 5000,    verticalPosition: 'bottom'
        });
      }
    },
    err => {
      this.matSnack.open('Please try after sometime', 'close', {
        duration: 5000,    verticalPosition: 'bottom'
      });
    })
  }

  onFileUpload = event => {

    this.adminService.submitForm(this.formGroup.value.file).subscribe((res: any) => {
        if (res.hrMasterDataSentToCPI) {
          this.matSnack.open('Form Uploaded successfully', 'close', {
            duration: 5000
          });
        } else {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000
          });
        }
      },
      err => {
        this.matSnack.open('Please try after sometime', 'close', {
          duration: 5000
        });
      })
  }

  downloadFile = e => {
    e.preventDefault()

    let downloadUrl = environment.hostName
    this.adminService.downloadFile().subscribe((res: any) => {
        console.log(res)
        let contentRes = []
        res.contentSlots.contentSlot.forEach(content => {
          if (content.slotId === 'HRServiceEmployeeMasterDatasetupSlot') {
            contentRes = content.components.component
          }
        });

        contentRes.forEach(content => {
          if (content.uid === 'HRServicesMasterDataExcelComponent') {
            downloadUrl += content.media.url
          }
        });

        window.open(downloadUrl, "_self")

      },
      err => {
        console.log(err)
      })
  }
}