import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HrmsFormMoleculeComponent } from './hrms-form-molecule.component';

describe('HrmsFormMoleculeComponent', () => {
  let component: HrmsFormMoleculeComponent;
  let fixture: ComponentFixture<HrmsFormMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HrmsFormMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HrmsFormMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
