import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { 
  AdminService,
  GENDERS,
  COUNTRIES,
  MARITAL_STATUS,
  RELIGIOUS,
  VALIDATION_MESSAGES } from '../../../services/admin.service'

@Component({
  selector: 'app-hrms-form-molecule',
  templateUrl: './hrms-form-molecule.component.html',
  styleUrls: ['./hrms-form-molecule.component.scss']
})
export class HrmsFormMoleculeComponent implements OnInit {

  genders: any
  countries: any
  maritalStatus: any
  religions: any
  personalInfo:any
  contactInfo: any
  employmentInfo: any
  contractDetails: any
  bankDetails: any
  validationMessages = VALIDATION_MESSAGES

  constructor(
    private fb: FormBuilder,
    private adminService: AdminService,
    private matSnack: MatSnackBar,
    ) { 
    this.genders = GENDERS
    this.countries = COUNTRIES
    this.maritalStatus = MARITAL_STATUS
    this.religions = RELIGIOUS
  }

  ngOnInit() {
    this.personalInfo = this.fb.group({
      fullName: [null, Validators.compose([Validators.required])],
      Fathername: [null, Validators.compose([Validators.required])],
      gender: [null, Validators.compose([Validators.required])],
      IqamaNo: [null, Validators.compose([Validators.required])],
      DOB: [null, Validators.compose([Validators.required])],
      religion: [null, Validators.compose([Validators.required])],
      maritalStatus: [null, Validators.compose([Validators.required])],
      nationality: [null, Validators.compose([Validators.required])]
    });

    this.contactInfo = this.fb.group({
      phoneNo: [null, Validators.compose([Validators.required])],
      email: [null, Validators.compose([Validators.required, Validators.email])]
    });

    this.employmentInfo = this.fb.group({
      empNo: [null, Validators.compose([Validators.required])],
      joiningDate: [null, Validators.compose([Validators.required])],
      jobTitle: [null, Validators.compose([Validators.required])],
      grade: [null, Validators.compose([Validators.required])],
      department: [null, Validators.compose([Validators.required])],
      division: [null, Validators.compose([Validators.required])],
      supEmpId: [null, Validators.compose([Validators.required])],
      location: [null, Validators.compose([Validators.required])],
      iqamaProfession: [null, Validators.compose([Validators.required])]
    });

    this.contractDetails = this.fb.group({
      basicSalary: [null, Validators.compose([Validators.required])],
      contractStatus: [null, Validators.compose([Validators.required])]
    });

    this.bankDetails = this.fb.group({
      bankName: [null, Validators.compose([Validators.required])],
      bankCode: [null, Validators.compose([Validators.required])],
      IBAN: [null, Validators.compose([Validators.required])]
    });
  }

  changeAccordian() {
    
  }

  onSubmitForm = () => {

    const { fullName, Fathername, gender, IqamaNo, nationality, DOB, maritalStatus, religion } = this.personalInfo.value

    const { empNo, joiningDate, iqamaProfession, jobTitle, grade, department, division, supEmpId, location } = this.employmentInfo.value

    const { basicSalary, contractStatus } = this.contractDetails.value

    const { phoneNo, email } = this.contactInfo.value

    const { bankName, bankCode, IBAN } = this.bankDetails.value

    if(
      this.contactInfo.valid &&
      this.employmentInfo.valid &&
      this.personalInfo.valid &&
      this.bankDetails.valid &&
      this.contractDetails.valid) {
      const request = {
        'personalInfo' :
            {
                'firstName' : fullName,
                'lastName' : fullName,
                'fatherName': Fathername,
                'gender' : gender,
                'iqamaNo' : IqamaNo,
                'nationality': nationality,
                'dateofBirth': `${DOB.getDate()}/${DOB.getMonth()}/${DOB.getFullYear()}`,
                'maritalStatus': maritalStatus,
                'religion': religion
            },
    
        'employmentInfo' :
            {
                'employeeNumber': empNo,
                'joiningDate': `${joiningDate.getDate()}/${joiningDate.getMonth()}/${joiningDate.getFullYear()}`,
                'professionOnIqama': iqamaProfession,
                'jobTilte': jobTitle,
                'grade': grade,
                'department': department,
                'division': division,
                'supervisorEmpID': supEmpId,
                'location': location
            },
        'contractDetails' :
            { 
                'basicSalary': basicSalary,
                'contractStatus': contractStatus
            },
        
        'contact' : 
            {
                'mobileNumber': phoneNo,
                'emailAddress': email
            },
    
        'bankDetails' :
            {
            'bankName': bankName,
            'bankCode': bankCode,
            'iBAN': IBAN
            }
    }

    this.adminService.submitForm(request).subscribe((res: any) => {
      if(res.active) {
        this.matSnack.open('Form submitted successfully', 'close', {
          duration: 5000,    verticalPosition: 'bottom'
        });
      } else {
        this.matSnack.open('Please try after sometime', 'close', {
          duration: 5000,    verticalPosition: 'bottom'
        });
      }
    },
    err => {
      this.matSnack.open('Please try after sometime', 'close', {
        duration: 5000,    verticalPosition: 'bottom'
      });
    })

    }
  }
}