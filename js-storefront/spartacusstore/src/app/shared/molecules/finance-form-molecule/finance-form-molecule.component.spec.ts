import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceFormMoleculeComponent } from './finance-form-molecule.component';

describe('FinanceFormMoleculeComponent', () => {
  let component: FinanceFormMoleculeComponent;
  let fixture: ComponentFixture<FinanceFormMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceFormMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceFormMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
