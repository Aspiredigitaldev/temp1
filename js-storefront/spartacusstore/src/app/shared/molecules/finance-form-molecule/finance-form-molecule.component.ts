import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { API_FAILURE_MSG, FinanceService, TRANSACTION_TYPES, CURRENCY, PO_CREATION_SUCCESS_MSG, PAYMENT_SUCCESS_MSG } from '../../../services/finance.service'

@Component({
  selector: 'app-finance-form-molecule',
  templateUrl: './finance-form-molecule.component.html',
  styleUrls: ['./finance-form-molecule.component.scss']
})
export class FinanceFormMoleculeComponent implements OnInit {

  @ViewChild('stepper', { static: false }) private myStepper: MatStepper;
  isLinear = false;
  POCreationFormGroup: FormGroup;
  materialReceivingFormGroup: FormGroup;
  paymentFormGroup: FormGroup;
  supliers: any
  itemCodes: any
  currencies: any
  poNumbers: any
  reviewExpanded: any = true
  isOptional: any = false
  reviewDate: any
  typesOfTransactions: any
  supplierReceived: any
  poTypeAtPayment: any

  constructor(
    private _formBuilder: FormBuilder,
    private financeService: FinanceService,
    public dialog: MatDialog,
    private matSnack: MatSnackBar, ) {
    this.currencies = CURRENCY
    this.typesOfTransactions = TRANSACTION_TYPES
  }

  ngOnInit() {

    this.financeService.getSupplierDetails().subscribe((res: any) => {
      console.log(res)
      if (res && res.supplierInfo && res.itemInfo) {
        this.supliers = res.supplierInfo
        this.itemCodes = res.itemInfo
        this.updateItemDetails(this.itemCodes[0])
        this.POCreationFormGroup.get('supplier').setValue(this.supliers[0].bpCode)
      }
    },
      err => {
        this.showErrorMsg(API_FAILURE_MSG)
      });
    this.financeService.getPurchaseOrder().subscribe((res: any) => {
      if (res && res.purchaseOrderList && res.purchaseOrderList.length > 0) {
        this.poNumbers = res.purchaseOrderList;
        this.updatePODetails(res.purchaseOrderList[0]);
      }
    });

    this.POCreationFormGroup = this._formBuilder.group({
      supplier: ['', Validators.required],
      itemCode: ['', Validators.required],
      itemDescription: ['', Validators.required],
      quantity: ['', Validators.required],
      unit: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      amount: [{ value: '', disabled: true }, Validators.required]
    });

    this.materialReceivingFormGroup = this._formBuilder.group({
      poNumber: ['', Validators.required],
      supplier: ['', Validators.required],
      itemCode: ['', Validators.required],
      itemDescription: ['', Validators.required],
      quantity: ['', Validators.required],
      unit: ['', Validators.required],
      currency: ['', Validators.required],
      price: ['', Validators.required],
      amount: ['', Validators.required],
      invoiceNumber: ['', Validators.required],
      invoiceAmount: ['', Validators.required],
      invoiceDate: ['', Validators.required],
      invoiceCurrency: ['', Validators.required],
    });

    this.paymentFormGroup = this._formBuilder.group({
      transactionEntryDate: ['', Validators.required],
      documentDate: ['', Validators.required],
      typeOfTransaction: ['', Validators.required],
      supplier: ['', Validators.required],
      amountInBankCurrency: ['', Validators.required],
      poNumber: ['', Validators.required],
      currency: ['', Validators.required]
    });

    // Used at payment
    this.financeService.getApprovedsupplier().subscribe((res: any) => {
      console.log(res)
      if(res && res.materialReceived.length> 0) {
        this.supplierReceived = res.materialReceived
      }
    },
    err => {
      this.showErrorMsg(API_FAILURE_MSG)
    })
  }

  updateAmount = () => {
    const quantity = this.POCreationFormGroup.get('quantity').value
    const price = this.POCreationFormGroup.get('price').value
    let amount = quantity * price
    this.POCreationFormGroup.get('amount').setValue(amount)
    this.POCreationFormGroup.get('quantity').setValue(quantity)
    this.POCreationFormGroup.get('price').setValue(price)
  }

  updateItemDetails = (itemData) => {

    this.POCreationFormGroup.setValue({
      supplier: this.POCreationFormGroup.get('supplier').value,
      itemDescription: itemData.description,
      unit: itemData.unitSet,
      itemCode: itemData.itemCode,
      quantity: this.POCreationFormGroup.get('quantity').value,
      currency: CURRENCY[0],
      price: this.POCreationFormGroup.get('price').value,
      amount: this.POCreationFormGroup.get('quantity').value * this.POCreationFormGroup.get('price').value
    })
  }

  updatePODetails = (data) => {

    this.materialReceivingFormGroup.setValue({
      poNumber: data.purchaseOrder,
      supplier: data.supplier,
      itemCode: data.item,
      itemDescription: data.itemDescription,
      quantity: data.quantity,
      unit: this.materialReceivingFormGroup.get('unit').value,
      currency: CURRENCY[0],
      price: data.price,
      amount: data.amount,
      invoiceDate: this.materialReceivingFormGroup.get('invoiceDate').value,
      invoiceNumber: this.materialReceivingFormGroup.get('invoiceNumber').value,
      invoiceAmount: this.materialReceivingFormGroup.get('invoiceAmount').value,
      invoiceCurrency: this.materialReceivingFormGroup.get('invoiceCurrency').value,

    })
  }

  submitInvoice = () => {

      const { supplier, itemCode, price, quantity, currency } = this.POCreationFormGroup.value

      const request = {
        'purchaseOrderData': {
          'supplier': supplier,
          'item': itemCode,
          'amount': this.POCreationFormGroup.get('amount').value,
          'price': price,
          'quantity': quantity,
          'currency': currency
        }
      }

      this.financeService.submitPurchaseOrder(request).subscribe((res: any) => {
        // this.financeService.getPurchaseOrder().subscribe((res: any) => {
        //   if (res && res.purchaseOrderList && res.purchaseOrderList.length > 0) {
        //     this.poNumbers = res.purchaseOrderList
        //     this.updatePODetails(res.purchaseOrderList[0])
        //   }
        // })
       // this.myStepper.next()
       this.showSuccessMsg('Form submitted successfully')
      },
        err => {
          this.showErrorMsg(API_FAILURE_MSG)
        })
  }

  submitReviewData = () => {

    const { invoiceNumber, poNumber, invoiceAmount, invoiceDate, currency } = this.materialReceivingFormGroup.value

    const request = {
      'materialReceivedData': {
        'purchaseOrder': poNumber,
        'invoiceNumber': invoiceNumber,
        'invoiceAmount': invoiceAmount,
        'invoiceDate':  `${invoiceDate.getDate()}/${invoiceDate.getMonth()}/${invoiceDate.getFullYear()}`,
        'currency': currency
      }
    }

    this.financeService.submitMaterialReceived(request).subscribe((res: any) => {
    //  this.showErrorMsg(PO_CREATION_SUCCESS_MSG)
    this.financeService.getApprovedsupplier().subscribe((res: any) => {
      console.log(res)
      if(res && res.materialReceived.length> 0) {
        this.supplierReceived = res.materialReceived
      }
    },
    err => {
      this.showErrorMsg(API_FAILURE_MSG)
    })
   // this.myStepper.next()

    },
    err => {
      this.showErrorMsg(API_FAILURE_MSG)
    })
  }

  moveToReview = () => {
    const { invoiceNumber, invoiceAmount, invoiceDate, invoiceCurrency, unit } = this.materialReceivingFormGroup.value

    this.materialReceivingFormGroup.get('invoiceNumber').setValue(invoiceNumber)
    this.materialReceivingFormGroup.get('invoiceAmount').setValue(invoiceAmount)
    this.materialReceivingFormGroup.get('invoiceCurrency').setValue(invoiceCurrency)
    this.materialReceivingFormGroup.get('unit').setValue(unit)
    this.reviewDate = `${invoiceDate.getDate()}/${invoiceDate.getMonth()}/${invoiceDate.getFullYear()}`
    this.myStepper.next()
  }

  updatePo = supplier => {
    this.financeService.getApprovedpurchaseOrder(supplier).subscribe((res: any) => {
      console.log(res)
      if(res && res.materialReceived.length > 0) {
        this.poTypeAtPayment = res.materialReceived
      }
    })
  }

  submitPayment = () => {

    const {supplier, documentDate, typeOfTransaction, poNumber, transactionEntryDate, amountInBankCurrency, currency} = this.paymentFormGroup.value

    const request = {
      'PaymentsData': {
        'supplier': supplier,
        'purchaseOrder': poNumber,
        'transactionEntryDate': `${transactionEntryDate.getDate()}/${transactionEntryDate.getMonth()}/${transactionEntryDate.getFullYear()}`,
        'documentDate':  `${documentDate.getDate()}/${documentDate.getMonth()}/${documentDate.getFullYear()}`,
        'transactionType': typeOfTransaction,
        'amount': amountInBankCurrency,
        'currency': currency
      }
    }

    this.financeService.submitPayment(request).subscribe((res: any) => {
      this.showErrorMsg(PAYMENT_SUCCESS_MSG)
    },
    err => {
      this.showErrorMsg(API_FAILURE_MSG)
    })

    
  }

  showErrorMsg = (errMsg) => {
    this.matSnack.open(errMsg, 'close', {
      duration: 5000, verticalPosition: 'bottom'
    });
  }
  showSuccessMsg = (succMsg) => {
    this.matSnack.open(succMsg, 'close', {
      duration: 5000, verticalPosition: 'bottom'
    });
  }
}
