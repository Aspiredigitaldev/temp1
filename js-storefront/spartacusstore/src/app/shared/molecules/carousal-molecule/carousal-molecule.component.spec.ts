import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarousalMoleculeComponent } from './carousal-molecule.component';

describe('CarousalMoleculeComponent', () => {
  let component: CarousalMoleculeComponent;
  let fixture: ComponentFixture<CarousalMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarousalMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarousalMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
