import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactUsMoleculeComponent } from './contact-us-molecule.component';

describe('ContactUsMoleculeComponent', () => {
  let component: ContactUsMoleculeComponent;
  let fixture: ComponentFixture<ContactUsMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactUsMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactUsMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
