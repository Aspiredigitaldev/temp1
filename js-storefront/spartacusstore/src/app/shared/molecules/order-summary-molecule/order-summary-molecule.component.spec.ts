import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSummaryMoleculeComponent } from './order-summary-molecule.component';

describe('OrderSummaryMoleculeComponent', () => {
  let component: OrderSummaryMoleculeComponent;
  let fixture: ComponentFixture<OrderSummaryMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSummaryMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSummaryMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
