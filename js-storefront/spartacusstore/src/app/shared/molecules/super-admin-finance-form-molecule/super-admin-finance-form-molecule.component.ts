import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import {
  SuperAdminService,
  VALIDATION_MESSAGES
} from '../../../services/super-admin.service';

@Component({
  selector: 'app-super-admin-finance-form-molecule',
  templateUrl: './super-admin-finance-form-molecule.component.html',
  styleUrls: ['./super-admin-finance-form-molecule.component.scss']
})
export class SuperAdminFinanceFormMoleculeComponent implements OnInit, OnChanges {
  genders: any;
  countries: any;
  maritalStatus: any;
  religions: any;

  customerInfo: FormGroup;
  supplierInfo: FormGroup;
  itemInfo: FormGroup;
  validationMessages = VALIDATION_MESSAGES;
  buttonName: any = 'Submit';
  constructor(private fb: FormBuilder,
              private superAdminService: SuperAdminService,
              private matSnack: MatSnackBar) { }

  ngOnInit() {
    this.customerInfo = this.fb.group({
      bpCode: [],
      name: [],
      address: [],
      vatNumber: [],
      currency: [],
      financialGroup: [],
    });

    this.supplierInfo = this.fb.group({
      bpCode: [],
      name: [],
      address: [],
      vatNumber: [],
      currency: [],
      financialGroup: [],
    });

    this.itemInfo = this.fb.group({
      itemCode: [],
      description: [],
      itemType: [],
      itemGroup: [],
      unitSet: ['100'],
      inventoryUnit: [],
      currency: [],
    });
    this.itemInfo.controls.unitSet.disable();
  }
  ngOnChanges() {

  }
  checkValues(val) {
    return val ? val + '' : '';
  }
  onSubmitForm = () => {

    if (
      this.customerInfo.valid &&
      this.supplierInfo.valid &&
      this.itemInfo.valid) {
      const request = {
        customerInfo:
        {
          bpCode: this.checkValues(this.customerInfo.value.bpCode) ,
          name: this.checkValues(this.customerInfo.value.name) ,
          address: this.checkValues(this.customerInfo.value.address) ,
          vatNumber: this.checkValues(this.customerInfo.value.vatNumber)  ,
          currency: this.checkValues(this.customerInfo.value.currency) ,
          financialGroup: this.checkValues(this.customerInfo.value.financialGroup)
        },
        supplierInfo:
       [ {
          bpCode:  this.checkValues(this.supplierInfo.value.bpCode) ,
          name:  this.checkValues(this.supplierInfo.value.name) ,
          address:  this.checkValues(this.supplierInfo.value.address) ,
          vatNumber:  this.checkValues(this.supplierInfo.value.vatNumber),
          currency:  this.checkValues(this.supplierInfo.value.currency) ,
          financialGroup:  this.checkValues(this.supplierInfo.value.financialGroup)
        }],
        itemInfo:
      [ {
          itemCode:  this.checkValues(this.itemInfo.value.itemCode) ,
          description:  this.checkValues(this.itemInfo.value.description) ,
          itemType:  this.checkValues(this.itemInfo.value.itemType) ,
          itemGroup:  this.checkValues(this.itemInfo.value.itemGroup) ,
          unitSet:  '100',
          inventoryUnit:  this.checkValues(this.itemInfo.value.inventoryUnit) ,
          currency:  this.checkValues(this.itemInfo.value.currency)
        }]

      };

      this.superAdminService.submitForm(request).subscribe((res: any) => {
        if (res) {
          this.matSnack.open('Form submitted successfully', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        } else {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        }
      },
        err => {
          this.matSnack.open('Please try after sometime', 'close', {
            duration: 5000, verticalPosition: 'top'
          });
        });

    }
  }

}
