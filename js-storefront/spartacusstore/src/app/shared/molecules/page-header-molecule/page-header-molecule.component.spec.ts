import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHeaderMoleculeComponent } from './page-header-molecule.component';

describe('PageHeaderMoleculeComponent', () => {
  let component: PageHeaderMoleculeComponent;
  let fixture: ComponentFixture<PageHeaderMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHeaderMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHeaderMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
