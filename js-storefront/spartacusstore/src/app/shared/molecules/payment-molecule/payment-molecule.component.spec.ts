import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentMoleculeComponent } from './payment-molecule.component';

describe('PaymentMoleculeComponent', () => {
  let component: PaymentMoleculeComponent;
  let fixture: ComponentFixture<PaymentMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
