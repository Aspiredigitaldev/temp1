import { Injectable } from '@angular/core';
import { CommonUtilityService } from './common-utility-service';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { PdpService } from './pdp.service';

import { Resolve } from '@angular/router';

@Injectable()
export class CartResolverService implements Resolve<any> {
  constructor(
    private utilityService: CommonUtilityService,
    private pdpService: PdpService) { }

  resolve() {

      const httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.utilityService.getCookie('AuthToken')}`);
        
    const url = `${ environment.hostName}${environment.cartAPI}${this.utilityService.getCookie('displayUid')}/carts`; 

    this.pdpService.checkCartExist(url).subscribe((data: any) => {

      const response = JSON.parse(JSON.stringify(data));

      if (response && response.carts && response.carts.length > 0 && response.carts[0].code) {

        const cartId = response.carts[0].code
        
        const cartApi = `${environment.cartAPI + this.utilityService.getCookie('displayUid')}/carts/${cartId}`;

        const httpHeaders = new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', `Bearer${this.utilityService.getCookie('token')}`);

        const options = {
          headers: httpHeaders
        };

        return this.utilityService.getRequest(cartApi, options);
      } else {
        return false;
      }
    },
    err => {
      return false;
    })
  }
}
