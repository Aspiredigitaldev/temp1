import { Injectable, ElementRef, Renderer2, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie';
import { of } from 'rxjs';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { environment } from '../../environments/environment';

/**
 * Utility service for the application
 * Common utility functions that can be used throughout the application 
 * @author Mohamed Omar Farook
 */
@Injectable()
export class CommonUtilityService {

    transferedState: any;
    constructor(
        @Inject(PLATFORM_ID) private plateformId: object,
        private http: HttpClient,
        private state: TransferState,
        private cookieService: CookieService,
        private router: Router) { }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or empty, otherwise false
     */
    isUndefinedOrEmpty(input: any) {
        if (undefined !== input && '' !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or null, otherwise false
     */
    isNullOrUndefined(input: any) {
        if (undefined !== input && null !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if application is running in browser
     */
    isBrowser(): boolean {
        if (isPlatformBrowser(this.plateformId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Endpoint url
     * @returns Get request response from server
     */
    getRequest(url: any, header) {
        let api = this.getApi(url)

        return this.http.get(api, header)
    }

    /**
     * @param Endpoint url
     * @returns Post request response from server
     */
    postRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.post(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns Put request response from server
     */
    putRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.put(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns patch request response from server
     */
    patchRequest(url, request, options) {
        let api = this.getApi(url)

        return this.http.patch(api, request, options)
    }

    /**
     * @param Endpoint url
     * @returns delete request response from server
     */
    deleteRequest(url, options) {
        let api = this.getApi(url)

        return this.http.delete(api, options)
    }

    /**
     * @param url
     * @returns generated endpoint url
     */
    getApi(url) {
        if (url === '../../assets/mockApi/home.json') {
            return url
        }
        return environment.hostName + url
    }

    getAuth() {
        const httpHeaders = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded;' });
        const url = environment.hostName + environment.oAuthAPI;
        const request = new HttpParams()
            .set('client_id', 'trusted_client')
            .set('client_secret', 'secret')
            .set('grant_type', 'client_credentials')
            .set('scope', 'extended');
        return this.http.post(url, request.toString(), { headers: httpHeaders }).subscribe((res: any) => {
            if (res && res.access_token) {
                this.setCookie('AuthToken', res.access_token);
            }
        });
    }

    setCookie(key: any, value: any) {
        this.cookieService.put(
            key,
            value,
            {
                path: '/',
                domain: window.location.hostname
            })
    }
    checkLoggedInOrRedirect() {
        if (this.getCookie('isAuthenticated')) {
            // logged in so return true
            return true;
        }
        // not logged in so redirect to login page with the return url and return false
        const routeState = this.router.routerState.snapshot;
        this.router.navigate(['login'], { queryParams: { returnUrl: routeState.url } });
        return false;
    }

    getCookie(key) {
        return this.cookieService.get(key);
    }

    removeCookie(key: any) {
        this.cookieService.remove(
            key,
            {
                path: '/',
                domain: window.location.hostname
            })
    }
    buildQueryParams(source: Object): HttpParams {
        let target: HttpParams = new HttpParams();
        Object.keys(source).forEach((key: string) => {
            const value: string | number | boolean | Date = source[key];
            if ((typeof value !== 'undefined') && (value !== null)) {
                target = target.append(key, value.toString());
            }
        });
        return target;
    }
}

