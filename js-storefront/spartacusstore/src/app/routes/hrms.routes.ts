import { NotFoundOrganismComponent } from '../shared/organisms/not-found-organism/not-found-organism.component';
import { AdminOrganismComponent } from '../shared/organisms/admin-organism/admin-organism.component';
import { FinanceOrganismComponent } from '../shared/organisms/finance-organism/finance-organism.component'

export const HRMS_ROUTES = [
  { path: '', redirectTo: 'hrms', pathMatch: 'full' },
  {
    path: 'hrms',
    component: AdminOrganismComponent,
    data: [{
      pageName: 'hrms Page',
    }],
  },
  {
    path: 'finance',
    component: FinanceOrganismComponent,
    data: [{
      pageName: 'Finance Page',
    }],
  }
]