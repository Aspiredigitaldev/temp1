import { NotFoundOrganismComponent } from '../shared/organisms/not-found-organism/not-found-organism.component';
import { FinanceMangerOrganismComponent } from '../shared/organisms/finance-manger-organism/finance-manger-organism.component';
export const FINANCE_MANAGER_ROUTES = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: 'dashboard',
        component: FinanceMangerOrganismComponent,
        data: [{
          pageName: 'Finance Manager Page',
        }],
      }
]