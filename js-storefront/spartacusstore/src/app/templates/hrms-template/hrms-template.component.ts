import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../services/common-utility-service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-hrms-template',
  templateUrl: './hrms-template.component.html',
  styleUrls: ['./hrms-template.component.scss']
})
export class HrmsTemplateComponent implements OnInit {

  userType: any
  pageName: any
  pageTitle: any

  constructor(
    private utilityService: CommonUtilityService,
    private router: Router) {

    // this.userType = this.utilityService.getCookie('adminType')

    // if(this.userType === undefined || this.userType === '') {
    //   this.router.navigate(['/'])
    // }

    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd) {
        let url = val.url

        switch(url) {
          case '/admin/hrms':
            this.pageName = 'HR'
            this.pageTitle = 'Master Data Setup'
            return
          case '/admin/finance':
            this.pageName = 'Finance'
            this.pageTitle = 'Accounts Payable'
            return
          default:
            this.pageName = 'HR'
            this.pageTitle = 'Master Data Setup'
            return
        }
      }
  });
   }

  ngOnInit() {
  }

}
