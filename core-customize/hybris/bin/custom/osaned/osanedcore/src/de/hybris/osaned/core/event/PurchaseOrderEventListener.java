/**
 *
 */
package de.hybris.osaned.core.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.log4j.Logger;



/**
 * @author balamurugan
 *
 */
public class PurchaseOrderEventListener extends AbstractEventListener<PurchaseOrderEvent>
{

	private final Logger LOG = Logger.getLogger(PurchaseOrderEventListener.class);

	@Override
	protected void onEvent(final PurchaseOrderEvent purchaseOrderEvent)
	{
		LOG.info("PurchaseOrderEventListerner");
	}

}
