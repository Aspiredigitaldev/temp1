/**
 *
 */
package de.hybris.osaned.core.customer.impl;



import de.hybris.osaned.core.cpi.CPIConnectionUtil;
import de.hybris.osaned.core.customer.OsanedFinanceB2BCustomerService;
import de.hybris.osaned.core.enums.B2BCustomerRoleEnum;
import de.hybris.osaned.core.enums.ItemTypeEnum;
import de.hybris.osaned.core.enums.TransactionTypeEnum;
import de.hybris.osaned.core.event.FinanceManagerEvent;
import de.hybris.osaned.core.event.PurchaseOrderEvent;
import de.hybris.osaned.core.event.VerificationEvent;
import de.hybris.osaned.core.model.CompanyInfoModel;
import de.hybris.osaned.core.model.FinanceCustomerMasterModel;
import de.hybris.osaned.core.model.FinanceManagerOpenBalanceModel;
import de.hybris.osaned.core.model.ItemInfoModel;
import de.hybris.osaned.core.model.MaterialReceivedModel;
import de.hybris.osaned.core.model.PaymentsModel;
import de.hybris.osaned.core.model.PurchaseOrderModel;
import de.hybris.platform.b2b.constants.B2BConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorservices.customer.impl.DefaultB2BCustomerAccountService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cmsfacades.dto.MediaFileDto;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedData;
import de.hybris.platform.commercefacades.user.data.PaymentsData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;
import de.hybris.platform.commerceservices.event.RegisterEvent;
import de.hybris.platform.commerceservices.security.SecureToken;
import de.hybris.platform.commerceservices.security.SecureTokenService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;


/**
 * @author balamurugan
 *
 */
public class DefaultOsanedFinanceB2BCustomerServiceImpl extends DefaultB2BCustomerAccountService
		implements OsanedFinanceB2BCustomerService
{
	@Resource
	private ModelService modelService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private SecureTokenService secureTokenService;
	@Resource
	private UserService userService;
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private CPIConnectionUtil cpiConnectionUtil;
	@Resource
	private EventService eventService;
	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private UnitService unitService;
	@Resource
	private MediaService mediaService;
	@Resource
	private KeyGenerator orderCodeGenerator;
	@Resource
	private CatalogVersionService catalogVersionService;
	private final Logger LOG = Logger.getLogger(DefaultOsanedFinanceB2BCustomerServiceImpl.class);

	@Override
	public void createFinanceCustomer(final B2BCustomerData b2bCustomerData)
	{

		final B2BCustomerModel b2bCustomerModel = modelService.create(B2BCustomerModel.class);
		b2bCustomerModel.setUid(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setEmail(b2bCustomerData.getEmail().toLowerCase());
		b2bCustomerModel.setName(b2bCustomerData.getName());
		b2bCustomerModel.setEmployeeId(b2bCustomerData.getEmployeeId());
		b2bCustomerModel.setIqamaId(b2bCustomerData.getIqamaId());
		b2bCustomerModel.setPassword(configurationService.getConfiguration().getString("b2bcustomer.defaultpassword"));
		b2bCustomerModel.setLoginDisabled(Boolean.TRUE);
		b2bCustomerModel.setSentToCPI(Boolean.FALSE);
		final UserGroupModel b2bCustomerGroup = userService.getUserGroupForUID(B2BConstants.B2BCUSTOMERGROUP);
		b2bCustomerModel.setDefaultB2BUnit(getFinanceB2BUnit(b2bCustomerData));
		b2bCustomerModel.setGroups(Collections.singleton(b2bCustomerGroup));
		final Set<B2BCustomerRoleEnum> B2BCustomerRoleEnumList = new HashSet<>();
		B2BCustomerRoleEnumList.add(enumerationService.getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(B2BCustomerRoleEnumList);
		modelService.save(b2bCustomerModel);
		setEmployeeInParentEmployee(b2bCustomerData, b2bCustomerModel);
		setParentEmloyeeInSubEmployee(b2bCustomerData, b2bCustomerModel);
		b2bCustomerModel.setVerificationUserId(b2bCustomerData.getVerificationUser());
		if (cpiConnectionUtil.sendDataToCpi(getConvertedJsonCustomerData(b2bCustomerModel)))
		{
			b2bCustomerModel.setSentToCPI(Boolean.TRUE);
			modelService.save(b2bCustomerModel);
		}
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected String generateSecureToken(final B2BCustomerData b2bCustomerData)
	{
		final SecureToken secureToken = new SecureToken(b2bCustomerData.getEmail().toLowerCase(), new Date().getTime());
		return secureTokenService.encryptData(secureToken);
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	protected String generateSecureToken(final B2BCustomerModel b2bCustomerModel)
	{
		final SecureToken secureToken = new SecureToken(b2bCustomerModel.getEmail().toLowerCase(), new Date().getTime());
		return secureTokenService.encryptData(secureToken);
	}

	/**
	 * @return
	 */
	protected String generateOtpNumber()
	{
		final Random random = new Random();
		return String.valueOf(random.nextInt(1000000));
	}

	/**
	 * @param b2bCustomerData
	 * @return
	 */
	private B2BUnitModel getFinanceB2BUnit(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel parentEmployee = userService.getUserForUID(b2bCustomerData.getFinaceCustomerId(),
				B2BCustomerModel.class);
		return parentEmployee.getDefaultB2BUnit();

	}

	/**
	 * @param b2bCustomerData
	 * @param b2bCustomerModel
	 */
	private void setEmployeeInParentEmployee(final B2BCustomerData b2bCustomerData, final B2BCustomerModel b2bCustomerModel)
	{

		final B2BCustomerModel parentEmployee = userService.getUserForUID(b2bCustomerData.getFinaceCustomerId(),
				B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(parentEmployee.getSubEmployeeIds()))
		{
			parentEmployee.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getEmail()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(parentEmployee.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getEmail());
			parentEmployee.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(parentEmployee);

	}

	/**
	 * @param b2bCustomerData
	 * @param b2bCustomerModel
	 */
	private void setParentEmloyeeInSubEmployee(final B2BCustomerData b2bCustomerData, final B2BCustomerModel b2bCustomerModel)
	{
		if (CollectionUtils.isEmpty(b2bCustomerModel.getSuperAdminEmployeeIds()))
		{
			b2bCustomerModel.setSuperAdminEmployeeIds(Collections.singleton(b2bCustomerData.getFinaceCustomerId()));
		}
		else
		{
			final Set<String> superEmployeeIdList = new HashSet<>(b2bCustomerModel.getSuperAdminEmployeeIds());
			superEmployeeIdList.add(b2bCustomerData.getHrCustomerId());
			b2bCustomerModel.setSuperAdminEmployeeIds(superEmployeeIdList);
		}
		getModelService().save(b2bCustomerModel);
	}

	/**
	 * @param b2bCustomerModel
	 * @return
	 */
	private String getConvertedJsonCustomerData(final B2BCustomerModel b2bCustomerModel)
	{
		final JSONObject json = new JSONObject();
		json.put("emailId", b2bCustomerModel.getEmail());
		json.put("name", b2bCustomerModel.getName());
		json.put("employeeId", b2bCustomerModel.getEmployeeId());
		json.put("iqamaId", b2bCustomerModel.getIqamaId());
		return json.toString();
	}

	/**
	 * @param event
	 * @param customerModel
	 * @return
	 */
	protected AbstractCommerceUserEvent initializeEvent(final AbstractCommerceUserEvent event,
			final B2BCustomerModel customerModel)
	{
		event.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		event.setSite(getBaseSiteService().getCurrentBaseSite());
		event.setCustomer(customerModel);
		event.setLanguage(getCommonI18NService().getCurrentLanguage());
		event.setCurrency(getCommonI18NService().getCurrentCurrency());
		return event;
	}

	@Override
	public void setAuthMatrixForUser(final String userId, final String approverLevel1, final String approverLevel2,
			final String approverLevel3)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		if (!StringUtils.isEmpty(approverLevel1))
		{
			b2bCustomerModel.setApproverLevel1(getB2BCustomerForUserId(approverLevel1));
		}
		if (!StringUtils.isEmpty(approverLevel2))
		{
			b2bCustomerModel.setApproverLevel2(getB2BCustomerForUserId(approverLevel2));
		}
		if (!StringUtils.isEmpty(approverLevel3))
		{
			b2bCustomerModel.setApproverLevel3(getB2BCustomerForUserId(approverLevel3));
		}
		modelService.save(b2bCustomerModel);
	}

	/**
	 * @param userId
	 * @return
	 */
	private B2BCustomerModel getB2BCustomerForUserId(final String userId)
	{
		return userService.getUserForUID(userId, B2BCustomerModel.class);

	}

	/**
	 * @param b2bCustomerModel
	 */
	private void setUserinFinanceManagerUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String financeManagerUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.financemanager.emailid");
		final B2BCustomerModel financeManagerUser = userService.getUserForUID(financeManagerUserId, B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(financeManagerUser.getSubEmployeeIds()))
		{
			financeManagerUser.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getUid()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(financeManagerUser.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getUid());
			financeManagerUser.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(financeManagerUser);
		eventService.publishEvent(initializeEvent(new FinanceManagerEvent(), financeManagerUser));
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void removeUserinVerificationUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String verificationUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.verification.emailid");
		final B2BCustomerModel verificationUser = userService.getUserForUID(verificationUserId, B2BCustomerModel.class);
		final Set<String> subEmployeeIdList = new HashSet<>(verificationUser.getSubEmployeeIds());
		subEmployeeIdList.remove(b2bCustomerModel.getUid());
		verificationUser.setSubEmployeeIds(subEmployeeIdList);
		modelService.save(verificationUser);
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void setUserinVerificationUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String verificationUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.verification.emailid");
		final B2BCustomerModel verificationUser = userService.getUserForUID(verificationUserId, B2BCustomerModel.class);
		if (CollectionUtils.isEmpty(verificationUser.getSubEmployeeIds()))
		{
			verificationUser.setSubEmployeeIds(Collections.singleton(b2bCustomerModel.getUid()));
		}
		else
		{
			final Set<String> subEmployeeIdList = new HashSet<>(verificationUser.getSubEmployeeIds());
			subEmployeeIdList.add(b2bCustomerModel.getUid());
			verificationUser.setSubEmployeeIds(subEmployeeIdList);
		}
		modelService.save(verificationUser);
		eventService.publishEvent(initializeEvent(new VerificationEvent(), verificationUser));
	}



	@Override
	public void setFinanceOpenBalance(final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = userService.getUserForUID(userId, B2BCustomerModel.class);
		setFinanceOpenBalanceDatatoCustomer(b2bCustomerModel, financeManagerOpenBalanceData);
		final ObjectMapper jsonMapper = new ObjectMapper();
		try
		{
			final String jsonData = jsonMapper.writeValueAsString(financeManagerOpenBalanceData);

			if (cpiConnectionUtil.sendDataToCpi(jsonData))
			{
				b2bCustomerModel.setFinanceOpenBalanceDataSentToCPI(Boolean.TRUE);
				removeUserinFinanceManagerUser(b2bCustomerModel);
				triggerEmailToAllSubEmployees(b2bCustomerModel);
				getModelService().save(b2bCustomerModel);
			}

		}
		catch (final JsonProcessingException e)
		{
			LOG.debug(e.getMessage());
			b2bCustomerModel.setFinanceOpenBalanceDataSentToCPI(Boolean.TRUE);
			getModelService().save(b2bCustomerModel);
		}
	}

	/**
	 * @param b2bCustomerModel
	 */
	private void triggerEmailToAllSubEmployees(final B2BCustomerModel b2bCustomerModel)
	{
		if (!CollectionUtils.isEmpty(b2bCustomerModel.getSubEmployeeIds()))
		{
			b2bCustomerModel.getSubEmployeeIds().forEach(emailId -> {
				final B2BCustomerModel subEmployee = userService.getUserForUID(emailId, B2BCustomerModel.class);
				subEmployee.setEmailVerificationToken(generateSecureToken(subEmployee));
				subEmployee.setOtp(generateOtpNumber());
				modelService.save(subEmployee);
				getEventService().publishEvent(initializeEvent(new RegisterEvent(), subEmployee));
			});
		}

	}

	/**
	 * @param b2bCustomerModel
	 */
	private void removeUserinFinanceManagerUser(final B2BCustomerModel b2bCustomerModel)
	{
		final String FinanceManagerUserId = configurationService.getConfiguration()
				.getString("b2bcustomer.account.financemanager.emailid");
		final B2BCustomerModel financeManagerUser = userService.getUserForUID(FinanceManagerUserId, B2BCustomerModel.class);
		final Set<String> subEmployeeIdList = new HashSet<>(financeManagerUser.getSubEmployeeIds());
		subEmployeeIdList.remove(b2bCustomerModel.getUid());
		financeManagerUser.setSubEmployeeIds(subEmployeeIdList);
		modelService.save(financeManagerUser);

	}

	/**
	 * @param b2bCustomerModel
	 * @param financeManagerOpenBalanceData
	 */
	private void setFinanceOpenBalanceDatatoCustomer(final B2BCustomerModel b2bCustomerModel,
			final FinanceManagerOpenBalanceData financeManagerOpenBalanceData)
	{
		final FinanceManagerOpenBalanceModel financeManagerOpenBalanceModel = modelService
				.create(FinanceManagerOpenBalanceModel.class);
		financeManagerOpenBalanceModel.setUid(b2bCustomerModel.getName() + generateOtpNumber());
		financeManagerOpenBalanceModel.setItemCode(financeManagerOpenBalanceData.getItemUpload().getItemCode());
		financeManagerOpenBalanceModel.setItemDescription(financeManagerOpenBalanceData.getItemUpload().getItemDescription());
		financeManagerOpenBalanceModel.setItemGroup(financeManagerOpenBalanceData.getItemUpload().getItemGroup());
		financeManagerOpenBalanceModel.setWarehouse(financeManagerOpenBalanceData.getItemUpload().getWarehouse());
		financeManagerOpenBalanceModel.setInventoryUnit(financeManagerOpenBalanceData.getItemUpload().getInventoryUnit());
		financeManagerOpenBalanceModel.setQuantity(financeManagerOpenBalanceData.getItemUpload().getQuantity());
		financeManagerOpenBalanceModel.setPrice(financeManagerOpenBalanceData.getItemUpload().getPrice());

		financeManagerOpenBalanceModel.setDocumentDate(financeManagerOpenBalanceData.getBpTransUpload().getDocumentDate());
		financeManagerOpenBalanceModel.setDocumentNumber(financeManagerOpenBalanceData.getBpTransUpload().getDocumentNumber());
		financeManagerOpenBalanceModel.setBusinessPartner(financeManagerOpenBalanceData.getBpTransUpload().getBusinessPartner());
		financeManagerOpenBalanceModel.setInvoiceNumber(financeManagerOpenBalanceData.getBpTransUpload().getInvoiceNumber());
		financeManagerOpenBalanceModel.setCurrency(financeManagerOpenBalanceData.getBpTransUpload().getCurrency());
		financeManagerOpenBalanceModel
				.setAmountInInvoiceCurrency(financeManagerOpenBalanceData.getBpTransUpload().getAmountInInvoiceCurrency());
		financeManagerOpenBalanceModel.setAmountInHC(financeManagerOpenBalanceData.getBpTransUpload().getAmountInHC());
		financeManagerOpenBalanceModel.setBalanceAmount(financeManagerOpenBalanceData.getBpTransUpload().getBalanceAmount());
		financeManagerOpenBalanceModel.setBpTransUploadReference(financeManagerOpenBalanceData.getBpTransUpload().getReference());

		financeManagerOpenBalanceModel.setDate(financeManagerOpenBalanceData.getOpeningBalance().getDate());
		financeManagerOpenBalanceModel.setLedger(financeManagerOpenBalanceData.getOpeningBalance().getLedger());
		financeManagerOpenBalanceModel.setDr(financeManagerOpenBalanceData.getOpeningBalance().getDr());
		financeManagerOpenBalanceModel.setAmount(financeManagerOpenBalanceData.getOpeningBalance().getAmount());
		financeManagerOpenBalanceModel.setOpeningBalanceReference(financeManagerOpenBalanceData.getOpeningBalance().getReference());
		modelService.save(financeManagerOpenBalanceModel);
		b2bCustomerModel.setFinanceOpeningBalance(financeManagerOpenBalanceModel);
		modelService.save(b2bCustomerModel);
	}

	@Override
	public void createCustomerMasterData(final CustomerFinanceMasterData customerFinanceMasterSetupData, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		FinanceCustomerMasterModel financeCustomerMaster = new FinanceCustomerMasterModel();
		financeCustomerMaster.setEmployee(b2bCustomerModel);
		try
		{
			financeCustomerMaster = flexibleSearchService.getModelByExample(financeCustomerMaster);

			final List<CompanyInfoModel> supplierInfoList = new ArrayList<>(financeCustomerMaster.getSupplierInfo());
			customerFinanceMasterSetupData.getSupplierInfo().forEach(supplierInfo -> {
				final CompanyInfoModel supplierInfoModel = modelService.create(CompanyInfoModel.class);
				supplierInfoModel.setBpCode(supplierInfo.getBpCode());
				supplierInfoModel.setName(supplierInfo.getName());
				supplierInfoModel.setAddress(supplierInfo.getAddress());
				supplierInfoModel.setVatNumber(supplierInfo.getVatNumber());
				supplierInfoModel.setCurrency(getCommonI18NService().getCurrency(supplierInfo.getCurrency()));
				supplierInfoModel.setFinancialGroup(supplierInfo.getFinancialGroup());
				supplierInfoList.add(supplierInfoModel);
			});
			financeCustomerMaster.setSupplierInfo(supplierInfoList);

			final List<ItemInfoModel> itemInfoList = new ArrayList<>(financeCustomerMaster.getItemInfo());
			customerFinanceMasterSetupData.getItemInfo().forEach(itemInfo -> {
				final ItemInfoModel itemInfoModel = modelService.create(ItemInfoModel.class);
				itemInfoModel.setItemCode(itemInfo.getItemCode());
				itemInfoModel.setDescription(itemInfo.getDescription());
				itemInfoModel.setType(enumerationService.getEnumerationValue(ItemTypeEnum.class, itemInfo.getItemType()));
				itemInfoModel.setItemGroup(itemInfo.getItemGroup());
				itemInfoModel.setUnitSet(Integer.valueOf(itemInfo.getUnitSet()));
				itemInfoModel.setInventoryUnit(unitService.getUnitForCode(itemInfo.getInventoryUnit()));
				itemInfoModel.setCurrency(getCommonI18NService().getCurrency(itemInfo.getCurrency()));
				itemInfoList.add(itemInfoModel);
			});
			financeCustomerMaster.setItemInfo(itemInfoList);
			modelService.save(financeCustomerMaster);
			sendFinanceMasterDataToCPI(customerFinanceMasterSetupData);
			setUserinVerificationUser(b2bCustomerModel);

		}
		catch (final Exception e)
		{
			final FinanceCustomerMasterModel financeCustomerMasterModel = modelService.create(FinanceCustomerMasterModel.class);
			financeCustomerMasterModel.setEmployee(b2bCustomerModel);

			final CompanyInfoModel companyInfoModel = modelService.create(CompanyInfoModel.class);
			companyInfoModel.setBpCode(customerFinanceMasterSetupData.getCustomerInfo().getBpCode());
			companyInfoModel.setName(customerFinanceMasterSetupData.getCustomerInfo().getName());
			companyInfoModel.setAddress(customerFinanceMasterSetupData.getCustomerInfo().getAddress());
			companyInfoModel.setVatNumber(customerFinanceMasterSetupData.getCustomerInfo().getVatNumber());
			companyInfoModel
					.setCurrency(getCommonI18NService().getCurrency(customerFinanceMasterSetupData.getCustomerInfo().getCurrency()));
			companyInfoModel.setFinancialGroup(customerFinanceMasterSetupData.getCustomerInfo().getFinancialGroup());
			financeCustomerMasterModel.setCustomerInfo(companyInfoModel);

			final List<CompanyInfoModel> supplierInfoList = new ArrayList<>();
			customerFinanceMasterSetupData.getSupplierInfo().forEach(supplierInfo -> {
				final CompanyInfoModel supplierInfoModel = modelService.create(CompanyInfoModel.class);
				supplierInfoModel.setBpCode(supplierInfo.getBpCode());
				supplierInfoModel.setName(supplierInfo.getName());
				supplierInfoModel.setAddress(supplierInfo.getAddress());
				supplierInfoModel.setVatNumber(supplierInfo.getVatNumber());
				supplierInfoModel.setCurrency(getCommonI18NService().getCurrency(supplierInfo.getCurrency()));
				supplierInfoModel.setFinancialGroup(supplierInfo.getFinancialGroup());
				supplierInfoList.add(supplierInfoModel);
			});
			financeCustomerMasterModel.setSupplierInfo(supplierInfoList);

			final List<ItemInfoModel> itemInfoList = new ArrayList<>();
			customerFinanceMasterSetupData.getItemInfo().forEach(itemInfo -> {
				final ItemInfoModel itemInfoModel = modelService.create(ItemInfoModel.class);
				itemInfoModel.setItemCode(itemInfo.getItemCode());
				itemInfoModel.setDescription(itemInfo.getDescription());
				itemInfoModel.setType(enumerationService.getEnumerationValue(ItemTypeEnum.class, itemInfo.getItemType()));
				itemInfoModel.setItemGroup(itemInfo.getItemGroup());
				itemInfoModel.setUnitSet(Integer.valueOf(itemInfo.getUnitSet()));
				itemInfoModel.setInventoryUnit(unitService.getUnitForCode(itemInfo.getInventoryUnit()));
				itemInfoModel.setCurrency(getCommonI18NService().getCurrency(itemInfo.getCurrency()));
				itemInfoList.add(itemInfoModel);
			});
			financeCustomerMasterModel.setItemInfo(itemInfoList);
			modelService.save(financeCustomerMasterModel);
			sendFinanceMasterDataToCPI(customerFinanceMasterSetupData);
			setUserinVerificationUser(b2bCustomerModel);
		}

	}

	@Override
	public void updateFinanceCustomer(final B2BCustomerData b2bCustomerData)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(b2bCustomerData.getEmail().toLowerCase(),
				B2BCustomerModel.class);
		final Set<B2BCustomerRoleEnum> customerRoles = new HashSet<>(b2bCustomerModel.getCustomerRole());
		customerRoles.add(enumerationService.getEnumerationValue(B2BCustomerRoleEnum.class, b2bCustomerData.getRole()));
		b2bCustomerModel.setCustomerRole(customerRoles);
		modelService.save(b2bCustomerModel);

	}

	private void sendFinanceMasterDataToCPI(final CustomerFinanceMasterData customerFinanceMasterSetupData)
	{
		final ObjectMapper jsonMapper = new ObjectMapper();
		try
		{
			final String jsonData = jsonMapper.writeValueAsString(customerFinanceMasterSetupData);

			if (cpiConnectionUtil.sendDataToCpi(jsonData))
			{
				LOG.debug("FinanceCustomerMasterData sent to CPI successfully");
			}

		}
		catch (final JsonProcessingException e)
		{
			LOG.debug(e.getMessage());
		}
	}

	@Override
	public FinanceCustomerMasterModel getCustomerMasterData(final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		final FinanceCustomerMasterModel financeCustomerMasterModel = new FinanceCustomerMasterModel();
		financeCustomerMasterModel.setEmployee(b2bCustomerModel);
		try
		{
			return flexibleSearchService.getModelByExample(financeCustomerMasterModel);
		}
		catch (final Exception e)
		{
			return null;
		}

	}

	@Override
	public void createPurchaseOrder(final PurchaseOrderData purchaseOrderData, final MediaFileDto media, final String userId)
	{
		final PurchaseOrderModel purchaseOrderModel = modelService.create(PurchaseOrderModel.class);
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		purchaseOrderModel.setEmployee(b2bCustomerModel);
		purchaseOrderModel.setCode(orderCodeGenerator.generate().toString());
		purchaseOrderModel.setQuantity(Long.valueOf(purchaseOrderData.getQuantity()));
		purchaseOrderModel.setPrice(BigDecimal.valueOf(Double.valueOf(purchaseOrderData.getPrice())));
		purchaseOrderModel.setAmount(BigDecimal.valueOf(Double.valueOf(purchaseOrderData.getAmount())));
		amountcheck(purchaseOrderData);
		if (null != media)
		{
			final MediaModel mediaModel = modelService.create(MediaModel.class);
			mediaModel.setCode(media.getName() + orderCodeGenerator.generate().toString());
			mediaModel.setRealFileName(media.getName());
			mediaModel.setSize(media.getSize());
			mediaModel.setMime(media.getMime());
			mediaModel.setCatalogVersion(catalogVersionService.getCatalogVersion("osanedContentCatalog", "Online"));
			modelService.save(mediaModel);
			populateStream(media, mediaModel);
			purchaseOrderModel.setAttachments(Collections.singletonList(mediaModel));
		}
		try
		{
			CompanyInfoModel companyInfoModel = new CompanyInfoModel();
			companyInfoModel.setBpCode(purchaseOrderData.getSupplier());
			companyInfoModel = flexibleSearchService.getModelByExample(companyInfoModel);
			purchaseOrderModel.setSupplier(companyInfoModel);
			ItemInfoModel itemInfoModel = new ItemInfoModel();
			itemInfoModel.setItemCode(purchaseOrderData.getItem());
			itemInfoModel = flexibleSearchService.getModelByExample(itemInfoModel);
			purchaseOrderModel.setItem(itemInfoModel);
			purchaseOrderModel.setCurrency(getCommonI18NService().getCurrency(purchaseOrderData.getCurrency()));
			purchaseOrderModel.setStatus(OrderStatus.CREATED);
			modelService.save(purchaseOrderModel);
			final PurchaseOrderEvent purchaseOrderEvent = new PurchaseOrderEvent();
			purchaseOrderEvent.setPurchaseOrder(purchaseOrderModel);
			getEventService().publishEvent(purchaseOrderEvent);
		}
		catch (final Exception e)
		{
			throw new IllegalArgumentException("Check you supplier BpCode or Item ItemCode");
		}

	}

	/**
	 * @param purchaseOrderData
	 */
	private void amountcheck(final PurchaseOrderData purchaseOrderData)
	{
		final BigDecimal price = BigDecimal.valueOf(Double.valueOf(purchaseOrderData.getPrice()));
		final Long quantity = Long.valueOf(purchaseOrderData.getQuantity());
		final BigDecimal amount = BigDecimal.valueOf(Double.valueOf(purchaseOrderData.getAmount()));
		final BigDecimal total = price.multiply(BigDecimal.valueOf(quantity));
		if (!amount.equals(total))
		{
			throw new IllegalArgumentException("Amount calculation is worng");
		}

	}

	private void populateStream(final MediaFileDto mediaFile, final MediaModel mediaModel)
	{
		try (InputStream inputStream = mediaFile.getInputStream())
		{
			mediaService.setStreamForMedia(mediaModel, inputStream);
		}
		catch (final IOException e)
		{
			LOG.info(e);
		}
	}

	@Override
	public List<PurchaseOrderModel> getPurchaseOrderData(final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		final PurchaseOrderModel purchaseOrderModel = new PurchaseOrderModel();
		purchaseOrderModel.setEmployee(b2bCustomerModel);
		purchaseOrderModel.setStatus(OrderStatus.APPROVED);
		try
		{
			return flexibleSearchService.getModelsByExample(purchaseOrderModel);

		}
		catch (final Exception e)
		{
			return Collections.EMPTY_LIST;
		}

	}

	@Override
	public void createMaterialReceivedData(final MaterialReceivedData materialReceivedData, final MediaFileDto media,
			final String userId)
	{
		PurchaseOrderModel purchaseOrderModel = new PurchaseOrderModel();
		purchaseOrderModel.setCode(materialReceivedData.getPurchaseOrder());
		try
		{

			purchaseOrderModel = flexibleSearchService.getModelByExample(purchaseOrderModel);
			final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
			final MaterialReceivedModel materialReceivedModel = getModelService().create(MaterialReceivedModel.class);
			materialReceivedModel.setEmployee(b2bCustomerModel);
			materialReceivedModel.setPurchaseOrder(purchaseOrderModel);
			materialReceivedModel.setInvoiceNumber(materialReceivedData.getInvoiceNumber());
			materialReceivedModel.setInvoiceAmount(BigDecimal.valueOf(Double.valueOf(materialReceivedData.getInvoiceAmount())));
			final Date invoiceDate = new SimpleDateFormat("MM/dd/yyyy").parse(materialReceivedData.getInvoiceDate());
			materialReceivedModel.setInvoiceDate(invoiceDate);
			if (null != media)
			{
				final MediaModel mediaModel = modelService.create(MediaModel.class);
				mediaModel.setCode(media.getName() + orderCodeGenerator.generate().toString());
				mediaModel.setRealFileName(media.getName());
				mediaModel.setSize(media.getSize());
				mediaModel.setMime(media.getMime());
				mediaModel.setCatalogVersion(catalogVersionService.getCatalogVersion("osanedContentCatalog", "Online"));
				modelService.save(mediaModel);
				populateStream(media, mediaModel);
				materialReceivedModel.setAttachments(Collections.singletonList(mediaModel));
			}
			materialReceivedModel.setStatus(OrderStatus.CREATED);
			materialReceivedModel.setCurrency(getCommonI18NService().getCurrency(materialReceivedData.getCurrency()));
			getModelService().save(materialReceivedModel);
		}
		catch (final Exception e)
		{
			throw new IllegalArgumentException(
					"MaterialReceived data alreday exist with PurchaseOrder code	: " + materialReceivedData.getPurchaseOrder());
		}

	}

	@Override
	public Set<CompanyInfoModel> getapprovedSupplier(final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		final MaterialReceivedModel materialReceivedModel = new MaterialReceivedModel();
		materialReceivedModel.setEmployee(b2bCustomerModel);
		materialReceivedModel.setStatus(OrderStatus.APPROVED);
		try
		{
			final List<MaterialReceivedModel> MaterialReceivedModelList = flexibleSearchService
					.getModelsByExample(materialReceivedModel);
			if (!CollectionUtils.isEmpty(MaterialReceivedModelList))
			{
				final Set<CompanyInfoModel> supplierInfo = new HashSet<>();
				MaterialReceivedModelList.forEach(mr -> {

					supplierInfo.add(mr.getPurchaseOrder().getSupplier());
				});
				return supplierInfo;
			}
		}
		catch (final Exception e)
		{
			return Collections.EMPTY_SET;
		}
		return null;
	}

	@Override
	public List<MaterialReceivedModel> getapprovedPurchaseOrder(final String userId, final String supplierCode)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		final MaterialReceivedModel materialReceivedModel = new MaterialReceivedModel();
		materialReceivedModel.setEmployee(b2bCustomerModel);
		materialReceivedModel.setStatus(OrderStatus.APPROVED);
		try
		{
			final List<MaterialReceivedModel> materialReceivedModelList = flexibleSearchService
					.getModelsByExample(materialReceivedModel);
			return materialReceivedModelList.stream()
					.filter(mr -> mr.getPurchaseOrder().getSupplier().getBpCode().equals(supplierCode)).collect(Collectors.toList());
		}
		catch (final Exception e)
		{
			return Collections.EMPTY_LIST;
		}
	}

	@Override
	public void createPaymentsData(final PaymentsData paymentsData, final MediaFileDto media, final String userId)
	{
		final B2BCustomerModel b2bCustomerModel = getUserService().getUserForUID(userId, B2BCustomerModel.class);
		CompanyInfoModel supplier = new CompanyInfoModel();
		supplier.setBpCode(paymentsData.getSupplier());
		supplier = flexibleSearchService.getModelByExample(supplier);
		PurchaseOrderModel purchaseOrder = new PurchaseOrderModel();
		purchaseOrder.setCode(paymentsData.getPurchaseOrder());
		purchaseOrder = flexibleSearchService.getModelByExample(purchaseOrder);
		final PaymentsModel paymentsModel = new PaymentsModel();
		paymentsModel.setSupplier(supplier);
		paymentsModel.setEmployee(b2bCustomerModel);
		Date transactionEntryDate = null;
		Date documentDate = null;
		List<PaymentsModel> paymentsModelList = Collections.EMPTY_LIST;
		try
		{
			transactionEntryDate = new SimpleDateFormat("MM/dd/yyyy").parse(paymentsData.getTransactionEntryDate());
			documentDate = new SimpleDateFormat("MM/dd/yyyy").parse(paymentsData.getDocumentDate());
			paymentsModelList = flexibleSearchService.getModelsByExample(paymentsModel);
		}
		catch (final Exception e)
		{
			LOG.debug(e);
		}

		if (!CollectionUtils.isEmpty(paymentsModelList))
		{
			checkForExistingPurchaseOrder(paymentsModelList, paymentsData);
		}
		final PaymentsModel newPaymentsModel = modelService.create(PaymentsModel.class);
		newPaymentsModel.setTransactionId(orderCodeGenerator.generate().toString());
		newPaymentsModel.setEmployee(b2bCustomerModel);
		newPaymentsModel.setSupplier(supplier);
		newPaymentsModel.setPurchaseOrder(Collections.singletonList(purchaseOrder));
		newPaymentsModel.setEntryDate(transactionEntryDate);
		newPaymentsModel.setDocumentDate(documentDate);
		newPaymentsModel.setTransactionType(
				enumerationService.getEnumerationValue(TransactionTypeEnum.class, paymentsData.getTransactionType()));
		newPaymentsModel.setAmount(BigDecimal.valueOf(Double.valueOf(paymentsData.getAmount())));
		if (null != media)
		{
			final MediaModel mediaModel = modelService.create(MediaModel.class);
			mediaModel.setCode(media.getName() + orderCodeGenerator.generate().toString());
			mediaModel.setRealFileName(media.getName());
			mediaModel.setSize(media.getSize());
			mediaModel.setMime(media.getMime());
			mediaModel.setCatalogVersion(catalogVersionService.getCatalogVersion("osanedContentCatalog", "Online"));
			modelService.save(mediaModel);
			populateStream(media, mediaModel);
			newPaymentsModel.setAttachments(Collections.singletonList(mediaModel));
		}
		newPaymentsModel.setCurrency(getCommonI18NService().getCurrency(paymentsData.getCurrency()));
		newPaymentsModel.setStatus(OrderStatus.CREATED);
		modelService.save(newPaymentsModel);
	}

	/**
	 * @param paymentsModelList
	 */
	private void checkForExistingPurchaseOrder(final List<PaymentsModel> paymentsModelList, final PaymentsData paymentsData)
	{
		paymentsModelList.forEach(payList -> {
			payList.getPurchaseOrder().forEach(p -> {
				if (p.getCode().equals(paymentsData.getPurchaseOrder()))
				{
					LOG.info("i am in");
					throw new IllegalArgumentException("Already payments Submitted for supplier : " + paymentsData.getSupplier()
							+ " and purchaseOrder : " + paymentsData.getPurchaseOrder());
				}
			});
		});
	}


}
