/**
 *
 */
package de.hybris.osaned.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;


/**
 * @author balamurugan
 *
 */
public class FinanceManagerEventListener extends AbstractAcceleratorSiteEventListener<FinanceManagerEvent>
{
	private final Logger LOG = Logger.getLogger(VerificationEventListener.class);
	@Resource
	private ModelService modelService;
	@Resource
	private BusinessProcessService businessProcessService;

	@Override
	protected SiteChannel getSiteChannelForEvent(final FinanceManagerEvent event)
	{
		final BaseSiteModel site = event.getSite();
		return site.getChannel();
	}

	@Override
	protected void onSiteEvent(final FinanceManagerEvent event)
	{
		final StoreFrontCustomerProcessModel storeFrontCustomerProcessModel = (StoreFrontCustomerProcessModel) businessProcessService
				.createProcess("financeManagerEmailProcess-" + event.getCustomer().getUid() + "-" + System.currentTimeMillis(),
						"financeManagerEmailProcess");
		storeFrontCustomerProcessModel.setCustomer(event.getCustomer());
		storeFrontCustomerProcessModel.setSite(event.getSite());
		storeFrontCustomerProcessModel.setStore(event.getBaseStore());
		storeFrontCustomerProcessModel.setCurrency(event.getCurrency());
		storeFrontCustomerProcessModel.setLanguage(event.getLanguage());
		modelService.save(storeFrontCustomerProcessModel);
		businessProcessService.startProcess(storeFrontCustomerProcessModel);
		LOG.info("started financeManagerEmailProcess Process");

	}

}
