/**
 *
 */
package de.hybris.osaned.core.customer;

import de.hybris.osaned.core.model.CompanyInfoModel;
import de.hybris.osaned.core.model.FinanceCustomerMasterModel;
import de.hybris.osaned.core.model.MaterialReceivedModel;
import de.hybris.osaned.core.model.PurchaseOrderModel;
import de.hybris.platform.cmsfacades.dto.MediaFileDto;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedData;
import de.hybris.platform.commercefacades.user.data.PaymentsData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;

import java.util.List;
import java.util.Set;


/**
 * @author balamurugan
 *
 */
public interface OsanedFinanceB2BCustomerService
{
	/**
	 * @param b2bCustomerData
	 */
	public void createFinanceCustomer(B2BCustomerData b2bCustomerData);

	/**
	 * @param b2bCustomerData
	 */
	public void updateFinanceCustomer(B2BCustomerData b2bCustomerData);


	/**
	 * @param userId
	 * @param approverLevel1
	 * @param approverLevel2
	 * @param approverLevel3
	 */
	public void setAuthMatrixForUser(String userId, String approverLevel1, String approverLevel2, String approverLevel3);

	/**
	 * @param customerFinanceMasterSetupData
	 * @param userId
	 */
	public void createCustomerMasterData(CustomerFinanceMasterData customerFinanceMasterSetupData, String userId);

	/**
	 * @param userId
	 * @return FinanceCustomerMasterModel
	 */
	public FinanceCustomerMasterModel getCustomerMasterData(String userId);

	/**
	 * @param FinanceManagerOpenBalanceData
	 * @param userId
	 */
	public void setFinanceOpenBalance(FinanceManagerOpenBalanceData financeManagerOpenBalanceData, String userId);

	/**
	 * @param purchaseOrderData
	 * @param media
	 */
	public void createPurchaseOrder(PurchaseOrderData purchaseOrderData, MediaFileDto media, String userId);

	/**
	 * @param userId
	 * @return PurchaseOrderData
	 */
	public List<PurchaseOrderModel> getPurchaseOrderData(String userId);

	/**
	 * @param materialReceivedData
	 */
	public void createMaterialReceivedData(MaterialReceivedData materialReceivedData, MediaFileDto media, String userId);

	/**
	 * @param userId
	 * @return CompanyInfoSet
	 */
	public Set<CompanyInfoModel> getapprovedSupplier(String userId);

	/**
	 * @param userId
	 * @return MaterialReceivedModelList
	 */
	public List<MaterialReceivedModel> getapprovedPurchaseOrder(String userId, String supplierCode);

	/**
	 * @param paymentsData
	 * @param userId
	 */
	public void createPaymentsData(PaymentsData paymentsData, MediaFileDto media, String userId);
}
