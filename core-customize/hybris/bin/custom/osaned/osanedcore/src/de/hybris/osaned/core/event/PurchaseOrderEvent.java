/**
 *
 */
package de.hybris.osaned.core.event;

import de.hybris.osaned.core.model.PurchaseOrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author balamurugan
 *
 */
public class PurchaseOrderEvent extends AbstractEvent
{
	private PurchaseOrderModel purchaseOrder;

	/**
	 * @return the purchaseOrder
	 */
	public PurchaseOrderModel getPurchaseOrder()
	{
		return purchaseOrder;
	}

	/**
	 * @param purchaseOrder
	 *           the purchaseOrder to set
	 */
	public void setPurchaseOrder(final PurchaseOrderModel purchaseOrder)
	{
		this.purchaseOrder = purchaseOrder;
	}


}
