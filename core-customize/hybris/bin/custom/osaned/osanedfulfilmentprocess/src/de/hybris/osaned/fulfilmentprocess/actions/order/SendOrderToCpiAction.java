/**
 *
 */
package de.hybris.osaned.fulfilmentprocess.actions.order;


import de.hybris.osaned.core.cpi.CPIConnectionUtil;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

import org.apache.commons.lang.StringUtils;

import net.minidev.json.JSONObject;


/**
 * @author Bala Murugan
 *
 */
public class SendOrderToCpiAction extends AbstractSimpleDecisionAction<OrderProcessModel>
{
	private CPIConnectionUtil cpiConnectionUtil;
	private EnumerationService enumerationService;

	@Override
	public Transition executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException, Exception
	{
		final OrderModel orderModel = orderProcessModel.getOrder();
		if (getCpiConnectionUtil().sendDataToCpi(getOrderJson(orderModel)))
		{
			orderModel.setSentToCPI(Boolean.TRUE);
			getModelService().save(orderModel);
			return Transition.OK;
		}
		else
		{
			orderModel.setSentToCPI(Boolean.FALSE);
			getModelService().save(orderModel);
			return Transition.NOK;
		}
	}

	/**
	 * @param orderModel
	 * @return
	 */
	private String getOrderJson(final OrderModel orderModel)
	{
		final JSONObject json = new JSONObject();
		if(null != orderModel)
		{
			json.put("orderId", orderModel.getCode());
			json.put("totalPrice",orderModel.getTotalPrice().toString());
			orderModel.getEntries().forEach(entry ->
			{
				json.put("productId", entry.getProduct().getCode());
				json.put("productName", entry.getProduct().getName());
				json.put("quantity", entry.getQuantity().toString());
			});
			final UserModel userModel = orderModel.getUser();
			if(userModel instanceof B2BCustomerModel)
			{
				final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userModel;
				if (null != b2bCustomerModel.getSizeRange())
				{
					final UserPriceGroup userPriceGroup = getEnumerationService().getEnumerationValue(UserPriceGroup.class,
							b2bCustomerModel.getSizeRange());
					json.put("sizeRange", getEnumerationService().getEnumerationName(userPriceGroup));
				}
			}
			return json.toString();
		}
		return StringUtils.EMPTY;
	}



	/**
	 * @return the cpiConnectionUtil
	 */
	public CPIConnectionUtil getCpiConnectionUtil()
	{
		return cpiConnectionUtil;
	}

	/**
	 * @param cpiConnectionUtil
	 *           the cpiConnectionUtil to set
	 */
	public void setCpiConnectionUtil(final CPIConnectionUtil cpiConnectionUtil)
	{
		this.cpiConnectionUtil = cpiConnectionUtil;
	}

	/**
	 * @return the enumerationService
	 */
	public EnumerationService getEnumerationService()
	{
		return enumerationService;
	}

	/**
	 * @param enumerationService
	 *           the enumerationService to set
	 */
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

}
