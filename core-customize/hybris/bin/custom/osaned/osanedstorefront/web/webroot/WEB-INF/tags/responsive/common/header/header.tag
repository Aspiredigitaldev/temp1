<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<spring:htmlEscape defaultHtmlEscape="true" />
<spring:url value="/login" var="loginUrl"/>
<spring:url value="/register" var="registerUrl"/>

<!------------------------------Header Starts ----------------------------------------->
  <header class="secondary-bg header_container"> 
    <section class="container">
       <div class="top_header">
		<ul class="top_section_left list-unstyled">
          <li class="text-white pr-20"><i class="top-icon glyphicon glyphicon-earphone primary-color"></i> +972-77-9322636</li>
          <li class="text-white"><i class="top-icon glyphicon glyphicon-envelope primary-color"></i> info@saned.com</li>
        </ul>
        <ul class="top_section_right list-unstyled no-padding">
          <li><a class="text-white text-uppercase pr-20" href="${loginUrl}">Login</a></li>
          <li><a class="text-white text-uppercase" href="${registerUrl}">Register</a></li>
        </ul>
      </div>
  </section>
  <section>
    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-inverse">
        <div class="container-fluid no-padding">
            <div class="col-xs-6 col-md-2">
           <cms:pageSlot position="SiteLogo" var="feature">
            	<c:if test="${not empty feature.media}">
            	<a class="site-logo" href="#"><img class="img-responsive" src="${feature.media.url}" alt="logo" /></a>
            	</c:if>
            </cms:pageSlot>
          </div>
          <div class="col-xs-2 navbar-header visible-xs">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
             
            <a class="text-white navbar-brand" href="#">SANED</a>
          </div>
         <c:set var="selected" value="1"/>
          <div id="navbar" class="navbar-collapse collapse no-padding">
          	 <cms:pageSlot position="NavigationBar" var="feature">
	            <ul class="col-xs-8 nav navbar-nav">
		            <c:if test="${feature.visible}">
			          	<c:if test="${not empty feature.navigationNode and not empty feature.navigationNode.children}">
			          		<c:forEach items="${feature.navigationNode.children}" var="childLevel1">
			          			<c:if test="${not empty childLevel1.entries}">
			          				<c:forEach items="${childLevel1.entries}" var="childLevel2">
			          					<c:if test="${not empty childLevel2.item and not empty childLevel2.item.linkName}">
				          					<c:choose>
				          						<c:when test="${selected eq 1}">
				          							<li class="active text-uppercase"><a href="#">${childLevel2.item.linkName}</a></li>
				          						 	<c:set var="selected" value="0"/>
				          						</c:when>
				          						<c:otherwise>
				          							<li><a class="text-white text-uppercase" href="#">${childLevel2.item.linkName}</a></li>
				          						</c:otherwise>
				          					</c:choose>
			          					</c:if>
		          					</c:forEach>
			          			</c:if>
		          			</c:forEach>
			          	</c:if>
		          	</c:if>
	            </ul>
             </cms:pageSlot>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div>
  </section>
  </header>
  <!------------------------------Headetr ends ----------------------------------------->


