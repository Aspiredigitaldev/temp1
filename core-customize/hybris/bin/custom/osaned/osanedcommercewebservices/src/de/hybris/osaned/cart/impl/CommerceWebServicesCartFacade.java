/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.osaned.cart.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.UserPriceGroup;

import javax.annotation.Resource;


/**
 * Extension of {@link DefaultCartFacade} for commercewebservices.
 */
public class CommerceWebServicesCartFacade extends DefaultCartFacade
{
	@Resource
	private EnumerationService enumerationService;

	@Override
	public CartData getSessionCart()
	{
		final CartData cartData;
		final CartModel cart = getCartService().getSessionCart();
		cartData = getCartConverter().convert(cart);
		return cartData;
	}

	/**
	 * Checks if given card belongs to anonymous user.
	 *
	 * @param cartGuid
	 *           GUID of the cart.
	 * @return <tt>true</tt> if the cart belongs to anonymous user.
	 */
	public boolean isAnonymousUserCart(final String cartGuid)
	{
		final CartModel cart = getCommerceCartService().getCartForGuidAndSiteAndUser(cartGuid,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getAnonymousUser());
		return cart != null;
	}

	/**
	 * Checks if given card belongs to current user.
	 *
	 * @param cartGuid
	 *           GUID of the cart.
	 * @return <tt>true</tt> if the cart belongs to current user.
	 */
	public boolean isCurrentUserCart(final String cartGuid)
	{
		final CartModel cart = getCommerceCartService().getCartForGuidAndSiteAndUser(cartGuid,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
		return cart != null;
	}

	/**
	 * setSizeRangeInCart
	 */
	public void setSizeRangeInCart()
	{
		final CartModel cart = getCartService().getSessionCart();
		final UserModel userModel = cart.getUser();
		if (userModel instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customer = (B2BCustomerModel) userModel;
			final UserPriceGroup priceGroup = enumerationService.getEnumerationValue(UserPriceGroup.class, customer.getSizeRange());
			cart.setEurope1PriceFactory_UPG(priceGroup);
			getModelService().save(cart);
		}

	}
}
