/**
 *
 */
package de.hybris.osaned.v2.controller;

import de.hybris.osaned.dto.customer.CustomerWsDTO;
import de.hybris.osaned.facades.customer.OsanedFinanceB2BCustomerFacade;
import de.hybris.platform.cmsfacades.dto.MediaFileDto;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedDataSet;
import de.hybris.platform.commercefacades.user.data.PaymentsData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderListData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.webservicescommons.cache.CacheControl;
import de.hybris.platform.webservicescommons.cache.CacheControlDirective;
import de.hybris.platform.webservicescommons.swagger.ApiBaseSiteIdAndUserIdParam;
import de.hybris.platform.webservicescommons.swagger.ApiFieldsParam;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * @author balamurugan
 *
 */
@Controller
@RequestMapping(value = "/{baseSiteId}/finance-portal/users")
@CacheControl(directive = CacheControlDirective.PRIVATE)
@Api(tags = "Finance-Portal")
public class OsanedFinancePortalController extends BaseCommerceController
{
	private static final Logger LOG = Logger.getLogger(OsanedFinancePortalController.class);
	@Resource(name = "wsCustomerFacade")
	private CustomerFacade customerFacade;
	@Resource
	private OsanedFinanceB2BCustomerFacade osanedFinanceB2BCustomerFacade;

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "{userId}/register", method = RequestMethod.POST)
	@ApiOperation(nickname = "create-HRCustomer", value = "Create HR Customer", notes = "Returns customer data.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public CustomerWsDTO createFinanceUser(@ApiParam(value = "Email Id", required = true)
	@RequestParam
	final String emailId, @ApiParam(value = "Name", required = true)
	@RequestParam
	final String name, @ApiParam(value = "Employee Id ", required = true)
	@RequestParam
	final String employeeId, @ApiParam(value = "Iqama Id", required = true)
	@RequestParam
	final String iqamaId, @ApiParam(value = "Customer Role", required = true)
	@RequestParam
	final String customerRole, @ApiParam(value = "Finance User Id", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Verification User Id", required = true)
	@RequestParam
	final String verificationUser, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException
	{
		Assert.hasText(emailId, "The field [emailId] cannot be empty");
		Assert.hasText(name, "The field [name] cannot be empty");
		Assert.hasText(employeeId, "The field [employeeId] cannot be empty");
		Assert.hasText(iqamaId, "The field [iqamaId] cannot be empty");
		Assert.hasText(customerRole, "The field [customerRole] cannot be empty");
		Assert.hasText(verificationUser, "The field [verificationUser] cannot be empty");
		final B2BCustomerData b2bCustomerData = createRegisterdata(emailId, name, employeeId, iqamaId, customerRole,
				verificationUser, userId);
		osanedFinanceB2BCustomerFacade.createFinanceCustomer(b2bCustomerData);
		final CustomerData customerData = customerFacade.getUserForUID(emailId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	/**
	 * @param emailId
	 * @param name
	 * @param employeeId
	 * @param iqamaId
	 * @return
	 */
	private B2BCustomerData createRegisterdata(final String emailId, final String name, final String employeeId,
			final String iqamaId, final String customerRole, final String verificationUser, final String financeCustomerId)
	{
		final B2BCustomerData b2bCustomerData = new B2BCustomerData();
		b2bCustomerData.setEmail(emailId);
		b2bCustomerData.setName(name);
		b2bCustomerData.setEmployeeId(employeeId);
		b2bCustomerData.setIqamaId(iqamaId);
		b2bCustomerData.setCompany(b2bCustomerData.getEmployeeId());
		b2bCustomerData.setRole(customerRole);
		b2bCustomerData.setFinaceCustomerId(financeCustomerId);
		b2bCustomerData.setVerificationUser(verificationUser);
		return b2bCustomerData;
	}

	@Secured(
	{ "ROLE_CUSTOMERGROUP", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "{userId}/setAuthMatrix", method = RequestMethod.PUT)
	@ApiOperation(nickname = "Set Authorization Matrix for User", value = " Authorization Matrix", notes = "Returns customer data.")
	@ApiBaseSiteIdAndUserIdParam
	@ResponseBody
	public CustomerWsDTO setAuthMatrixForUser(@ApiParam(value = "Level-1 Approver", required = false)
	@RequestParam
	final String approverLevel1, @ApiParam(value = "Level-2 Approver", required = false)
	@RequestParam
	final String approverLevel2, @ApiParam(value = "Level-3 Approver", required = false)
	@RequestParam
	final String approverLevel3, @ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields) throws DuplicateUidException
	{
		osanedFinanceB2BCustomerFacade.setAuthMatrixForUser(userId, approverLevel1, approverLevel2, approverLevel3);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customermasterdatasetup", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "FinanceCustomerMasterDataSetupToCPI", value = "Finance SuperAdmin submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "Customer : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "Supplier : bpCode,name,address,vatNumber,currency,financialGroup"
			+ "item : itemCode,description,itemType,itemGroup,unitSet,inventoryUnit,currency")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createcustomerFinanceMasterData(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Customer FinanceMasterSetup Object", required = true)
	@RequestBody
	final CustomerFinanceMasterData customerFinanceMasterSetupData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedFinanceB2BCustomerFacade.createCustomerMasterData(customerFinanceMasterSetupData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/customermasterdatasetup", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "GET FinanceCustomerMasterDataSetup", value = " Retrieve Finance SuperAdmin", notes = "Get FinanceCustomerMasterDataSetup from Finane SuperAdmin ")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerFinanceMasterData getFinanceMasterSetup(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		final CustomerFinanceMasterData customerFinanceMasterSetupData = osanedFinanceB2BCustomerFacade
				.getCustomerMasterData(userId);
		return getDataMapper().map(customerFinanceMasterSetupData, CustomerFinanceMasterData.class, fields);
	}


	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/finance-openbalance", method = RequestMethod.POST, consumes =
	{ MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "FinanceOpenBalanceToCPI", value = "FinanceManager submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "ItemUpload : itemCode,itemDescription,itemGroup,warehouse,inventoryUnit,quantity,price"
			+ "BPTransUpload : documentNumber,documentDate,businessPartner,invoiceNumber,currency,amountInInvoiceCurrency,ammountInHC,balanceAmount,reference"
			+ "OpeningBalance -> date, ledger,dr,amount,reference")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createFinanceOpenBalance(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "Employe MasterDataSetup Object", required = true)
	@RequestBody
	final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse)
	{
		osanedFinanceB2BCustomerFacade.setFinanceOpenBalance(financeManagerOpenBalanceData, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/purchase-order", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "PurchaseOrderToCPI", value = "SubEmployee submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "PurchaseOrderData -> supplier,item,quantity,amount,price,currency" + "Attachment")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createPurchaseOrder(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "PurchaseOrderData", required = true)
	@RequestPart(value = "purchaseOrderData")
	final PurchaseOrderData purchaseOrderData, @ApiParam(value = "Attachment", required = false)
	@RequestPart(value = "attachment", required = false)
	final MultipartFile attachment, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		Assert.hasText(purchaseOrderData.getSupplier(), "The field [Supplier] cannot be empty");
		Assert.hasText(purchaseOrderData.getItem(), "The field [Item] cannot be empty");
		Assert.hasText(purchaseOrderData.getPrice(), "The field [Price] cannot be empty");
		Assert.hasText(purchaseOrderData.getAmount(), "The field [Amount] cannot be empty");
		Assert.hasText(purchaseOrderData.getQuantity(), "The field [Quantity] cannot be empty");
		Assert.hasText(purchaseOrderData.getCurrency(), "The field [Curency] cannot be empty");
		MediaFileDto media = null;
		if (null != attachment)
		{
			media = getFile(attachment, attachment.getInputStream());
		}
		osanedFinanceB2BCustomerFacade.createPurchaseOrder(purchaseOrderData, media, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	public MediaFileDto getFile(final MultipartFile file, final InputStream inputStream)
	{
		final MediaFileDto mediaFile = new MediaFileDto();
		mediaFile.setInputStream(inputStream);
		mediaFile.setName(file.getOriginalFilename());
		mediaFile.setSize(file.getSize());
		mediaFile.setMime(file.getContentType());
		return mediaFile;
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/purchase-order", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "PurchaseOrderDataList", value = "Get PurchaseOrderDataList", notes = "Get PurchaseOrderDataList")
	@ApiBaseSiteIdAndUserIdParam
	public PurchaseOrderListData getPurchaseOrder(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		return getDataMapper().map(osanedFinanceB2BCustomerFacade.getPurchaseOrderData(userId), PurchaseOrderListData.class,
				fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/material-received", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "MaterialReceivedDataToCPI", value = "SubEmployee submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "MaterialReceivedData -> purchaseOrder,supplier,item,quantity,amount,price,currency,InvoiceNumber,InvoiceDate,InvoiceDate"
			+ "Attachment")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createMaterialReceived(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "materialReceivedData", required = true)
	@RequestPart(value = "materialReceivedData")
	final MaterialReceivedData materialReceivedData, @ApiParam(value = "Attachment", required = false)
	@RequestPart(value = "attachment", required = false)
	final MultipartFile attachment, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		Assert.hasText(materialReceivedData.getPurchaseOrder(), "The field [PurchaseOrder] cannot be empty");
		Assert.hasText(materialReceivedData.getInvoiceNumber(), "The field [InvoiceNumber] cannot be empty");
		Assert.hasText(materialReceivedData.getInvoiceDate(), "The field [InvoiceDate] cannot be empty");
		Assert.hasText(materialReceivedData.getInvoiceAmount(), "The field [InvoiceAmount] cannot be empty");
		Assert.hasText(materialReceivedData.getCurrency(), "The field [Currency] cannot be empty");
		MediaFileDto media = null;
		if (null != attachment)
		{
			media = getFile(attachment, attachment.getInputStream());
		}
		osanedFinanceB2BCustomerFacade.createMaterialReceivedData(materialReceivedData, media, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/approvedsuppliers", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "Get approvedsuppliers", value = "Get approvedsuppliers", notes = "Get approvedsuppliers")
	@ApiBaseSiteIdAndUserIdParam
	public MaterialReceivedDataSet getapprovedsuppliers(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		return getDataMapper().map(osanedFinanceB2BCustomerFacade.getapprovedSuppliers(userId), MaterialReceivedDataSet.class,
				fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/approvedpurchaseorder/{supplierCode}", method = RequestMethod.GET)
	@ResponseBody
	@ApiOperation(nickname = "Get approvedPurchaseOrder", value = "Get approvedPurchaseOrder", notes = "Get approvedPurchaseOrder")
	@ApiBaseSiteIdAndUserIdParam
	public MaterialReceivedDataSet getApprovedPurchaseOrder(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "supplierCode", required = true)
	@PathVariable
	final String supplierCode, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		return getDataMapper().map(osanedFinanceB2BCustomerFacade.getapprovedPurchaseOrder(userId, supplierCode),
				MaterialReceivedDataSet.class, fields);
	}

	@Secured(
	{ "ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "ROLE_CUSTOMERMANAGERGROUP" })
	@RequestMapping(value = "/{userId}/payments", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	@ApiOperation(nickname = "PaymentsDataToCPI", value = "SubEmployee submits the data to CPI", notes = "To send data to CPI . Requires the following "
			+ "PaymentsData -> transactionEntryDate,documentDate,transactionType,purchaseOrder,supplier,amount,currency"
			+ "Attachment")
	@ApiBaseSiteIdAndUserIdParam
	public CustomerWsDTO createPaymentsData(@ApiParam(value = "userId", required = true)
	@PathVariable
	final String userId, @ApiParam(value = "PaymentsData", required = true)
	@RequestPart(value = "PaymentsData")
	final PaymentsData paymentsData, @ApiParam(value = "Attachment", required = false)
	@RequestPart(value = "attachment", required = false)
	final MultipartFile attachment, @ApiFieldsParam
	@RequestParam(defaultValue = DEFAULT_FIELD_SET)
	final String fields, final HttpServletRequest httpRequest, final HttpServletResponse httpResponse) throws IOException
	{
		Assert.hasText(paymentsData.getPurchaseOrder(), "The field [PurchaseOrder] cannot be empty");
		Assert.hasText(paymentsData.getSupplier(), "The field [Supplier] cannot be empty");
		Assert.hasText(paymentsData.getTransactionEntryDate(), "The field [TransactionEntryDate] cannot be empty");
		Assert.hasText(paymentsData.getDocumentDate(), "The field [DocumentDate] cannot be empty");
		Assert.hasText(paymentsData.getTransactionType(), "The field [TransactionType] cannot be empty");
		Assert.hasText(paymentsData.getAmount(), "The field [Amount] cannot be empty");
		Assert.hasText(paymentsData.getCurrency(), "The field [Currency] cannot be empty");
		MediaFileDto media = null;
		if (null != attachment)
		{
			media = getFile(attachment, attachment.getInputStream());
		}
		osanedFinanceB2BCustomerFacade.createPaymentsData(paymentsData, media, userId);
		final CustomerData customerData = customerFacade.getUserForUID(userId);
		return getDataMapper().map(customerData, CustomerWsDTO.class, fields);
	}
}
