/**
 *
 */
package de.hybris.osaned.facades.populators;

import de.hybris.osaned.core.model.HREmployeeMasterDataModel;
import de.hybris.platform.commercefacades.user.data.EmployeeMasterSetupData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;


/**
 * @author balamurugan
 *
 */
public class HRMasterDataReversePopulator implements Populator<EmployeeMasterSetupData, HREmployeeMasterDataModel>
{
	@Resource
	private ModelService modelService;

	@Override
	public void populate(final EmployeeMasterSetupData source, final HREmployeeMasterDataModel target) throws ConversionException
	{
		Assert.assertNotNull("Source is null", source);
		if (null != source.getPersonalInfo() && null != source.getEmploymentInfo() && null != source.getContractDetails()
				&& null != source.getContact() && null != source.getBankDetails())
		{
			final HREmployeeMasterDataModel hrEmployeeMasterDataModel = modelService.create(HREmployeeMasterDataModel.class);
			hrEmployeeMasterDataModel.setIqamaNo(source.getPersonalInfo().getIqamaNo());
			hrEmployeeMasterDataModel.setFirstName(source.getPersonalInfo().getFirstName());
			hrEmployeeMasterDataModel.setLastName(source.getPersonalInfo().getLastName());
			hrEmployeeMasterDataModel.setFatherName(source.getPersonalInfo().getFatherName());
			hrEmployeeMasterDataModel.setGender(source.getPersonalInfo().getGender());
			hrEmployeeMasterDataModel.setNationality(source.getPersonalInfo().getNationality());
			hrEmployeeMasterDataModel.setDateofBirth(source.getPersonalInfo().getDateofBirth());
			hrEmployeeMasterDataModel.setMaritalStatus(source.getPersonalInfo().getMaritalStatus());
			hrEmployeeMasterDataModel.setReligion(source.getPersonalInfo().getReligion());
			hrEmployeeMasterDataModel.setEmployeeNumber(source.getEmploymentInfo().getEmployeeNumber());
			hrEmployeeMasterDataModel.setJoiningDate(source.getEmploymentInfo().getJoiningDate());
			hrEmployeeMasterDataModel.setProfessionOnIqama(source.getEmploymentInfo().getProfessionOnIqama());
			hrEmployeeMasterDataModel.setJobTilte(source.getEmploymentInfo().getJobTilte());
			hrEmployeeMasterDataModel.setGrade(source.getEmploymentInfo().getGrade());
			hrEmployeeMasterDataModel.setDepartment(source.getEmploymentInfo().getDepartment());
			hrEmployeeMasterDataModel.setDivision(source.getEmploymentInfo().getDivision());
			hrEmployeeMasterDataModel.setSupervisorEmpID(source.getEmploymentInfo().getSupervisorEmpId());
			hrEmployeeMasterDataModel.setLocation(source.getEmploymentInfo().getLocation());
			hrEmployeeMasterDataModel.setBasicSalary(source.getContractDetails().getBasicSalary());
			hrEmployeeMasterDataModel.setContractStatus(source.getContractDetails().getContractStatus());
			hrEmployeeMasterDataModel.setMobileNumber(source.getContact().getMobileNumber());
			hrEmployeeMasterDataModel.setEmailAddress(source.getContact().getEmailAddress());
			hrEmployeeMasterDataModel.setBankName(source.getBankDetails().getBankName());
			hrEmployeeMasterDataModel.setBankCode(source.getBankDetails().getBankCode());
			hrEmployeeMasterDataModel.setIBAN(source.getBankDetails().getIban());
			modelService.save(hrEmployeeMasterDataModel);

		}
	}

}
