/**
 *
 */
package de.hybris.osaned.facades.customer.impl;

import de.hybris.osaned.core.customer.OsanedFinanceB2BCustomerService;
import de.hybris.osaned.core.model.CompanyInfoModel;
import de.hybris.osaned.core.model.FinanceCustomerMasterModel;
import de.hybris.osaned.core.model.MaterialReceivedModel;
import de.hybris.osaned.core.model.PurchaseOrderModel;
import de.hybris.osaned.facades.customer.OsanedFinanceB2BCustomerFacade;
import de.hybris.platform.cmsfacades.dto.MediaFileDto;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedDataSet;
import de.hybris.platform.commercefacades.user.data.PaymentsData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderListData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.core.convert.converter.Converter;
import org.springframework.util.CollectionUtils;


/**
 * @author balamurugan
 *
 */
public class DefaultOsanedFinanceB2BCustomerFacadeImpl implements OsanedFinanceB2BCustomerFacade
{
	@Resource
	private UserFacade userFacade;
	@Resource
	private OsanedFinanceB2BCustomerService osanedFinanceB2BCustomerService;
	@Resource
	private Converter<FinanceCustomerMasterModel, CustomerFinanceMasterData> customerFinanceMasterSetupDataConverter;
	@Resource
	private Converter<PurchaseOrderModel, PurchaseOrderData> purchaseOrderDataConverter;

	@Override
	public void createFinanceCustomer(final B2BCustomerData b2bCustomerData) throws DuplicateUidException
	{
		if (!userFacade.isUserExisting(b2bCustomerData.getEmail()))
		{
			osanedFinanceB2BCustomerService.createFinanceCustomer(b2bCustomerData);
		}
		else
		{
			osanedFinanceB2BCustomerService.updateFinanceCustomer(b2bCustomerData);
		}
	}

	@Override
	public void setAuthMatrixForUser(final String userId, final String approverLevel1, final String approverLevel2,
			final String approverLevel3)
	{

		osanedFinanceB2BCustomerService.setAuthMatrixForUser(userId, approverLevel1, approverLevel2, approverLevel3);
	}


	@Override
	public void setFinanceOpenBalance(final FinanceManagerOpenBalanceData financeManagerOpenBalanceData, final String userId)
	{
		osanedFinanceB2BCustomerService.setFinanceOpenBalance(financeManagerOpenBalanceData, userId);
	}

	@Override
	public void createCustomerMasterData(final CustomerFinanceMasterData customerFinanceMasterSetupData, final String userId)
	{
		osanedFinanceB2BCustomerService.createCustomerMasterData(customerFinanceMasterSetupData, userId);
	}

	@Override
	public CustomerFinanceMasterData getCustomerMasterData(final String userId)
	{
		if (null != osanedFinanceB2BCustomerService.getCustomerMasterData(userId))
		{
			return customerFinanceMasterSetupDataConverter.convert(osanedFinanceB2BCustomerService.getCustomerMasterData(userId));
		}
		return null;
	}

	@Override
	public void createPurchaseOrder(final PurchaseOrderData purchaseOrderData, final MediaFileDto media, final String userId)
	{
		osanedFinanceB2BCustomerService.createPurchaseOrder(purchaseOrderData, media, userId);
	}

	@Override
	public PurchaseOrderListData getPurchaseOrderData(final String userId)
	{
		if (!CollectionUtils.isEmpty(osanedFinanceB2BCustomerService.getPurchaseOrderData(userId)))
		{
			final List<PurchaseOrderModel> purchaseOrderList = osanedFinanceB2BCustomerService.getPurchaseOrderData(userId);
			final PurchaseOrderListData purchaseOrderListData = new PurchaseOrderListData();
			final List<PurchaseOrderData> PurchaseOrderList = new ArrayList<>();
			purchaseOrderList.forEach(purchaseOrder -> {
				PurchaseOrderData purchaseOrderData = new PurchaseOrderData();
				purchaseOrderData = purchaseOrderDataConverter.convert(purchaseOrder);
				PurchaseOrderList.add(purchaseOrderData);
			});
			purchaseOrderListData.setPurchaseOrderList(PurchaseOrderList);
			return purchaseOrderListData;
		}
		return new PurchaseOrderListData();
	}

	@Override
	public void createMaterialReceivedData(final MaterialReceivedData materialReceivedData, final MediaFileDto media,
			final String userId)
	{
		osanedFinanceB2BCustomerService.createMaterialReceivedData(materialReceivedData, media, userId);
	}


	@Override
	public MaterialReceivedDataSet getapprovedSuppliers(final String userId)
	{
		final Set<CompanyInfoModel> companyInfoModelSet = osanedFinanceB2BCustomerService.getapprovedSupplier(userId);
		if (!CollectionUtils.isEmpty(companyInfoModelSet))
		{
			final MaterialReceivedDataSet materialReceivedDataSet = new MaterialReceivedDataSet();
			final Set<MaterialReceivedData> MaterialReceivedData = new HashSet<>();
			companyInfoModelSet.forEach(s -> {
				final MaterialReceivedData materialReceived = new MaterialReceivedData();
				materialReceived.setSupplier(s.getBpCode());
				MaterialReceivedData.add(materialReceived);
			});
			materialReceivedDataSet.setMaterialReceived(MaterialReceivedData);
			return materialReceivedDataSet;
		}
		return null;
	}

	@Override
	public MaterialReceivedDataSet getapprovedPurchaseOrder(final String userId, final String supplierCode)
	{
		if (!CollectionUtils.isEmpty(osanedFinanceB2BCustomerService.getapprovedPurchaseOrder(userId, supplierCode)))
		{
			final Set<MaterialReceivedData> materialReceivedDataSet = new HashSet<MaterialReceivedData>();
			final MaterialReceivedDataSet finalMaterialReceivedDataSet = new MaterialReceivedDataSet();
			final List<MaterialReceivedModel> MaterialReceivedModelList = osanedFinanceB2BCustomerService
					.getapprovedPurchaseOrder(userId, supplierCode);
			MaterialReceivedModelList.forEach(mr -> {
				final MaterialReceivedData materialReceivedData = new MaterialReceivedData();
				materialReceivedData.setPurchaseOrder(mr.getPurchaseOrder().getCode());
				materialReceivedDataSet.add(materialReceivedData);
			});
			finalMaterialReceivedDataSet.setMaterialReceived(materialReceivedDataSet);
			return finalMaterialReceivedDataSet;
		}
		return null;
	}

	@Override
	public void createPaymentsData(final PaymentsData paymentsData, final MediaFileDto media, final String userId)
	{
		osanedFinanceB2BCustomerService.createPaymentsData(paymentsData, media, userId);
	}

}
