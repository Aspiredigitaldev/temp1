/**
 *
 */
package de.hybris.osaned.facades.customer.impl;

import de.hybris.osaned.core.customer.OsanedB2BCustomerAccountService;
import de.hybris.osaned.core.customer.OsanedB2BCustomerService;
import de.hybris.osaned.core.exception.CPIConnectionFailureException;
import de.hybris.osaned.facades.customer.OsanedB2BCustomerFacade;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author bala.v
 *
 */
public class OsanedDefaultB2BCustomerFacade extends DefaultB2BCustomerFacade implements OsanedB2BCustomerFacade
{

	private OsanedB2BCustomerAccountService osanedB2BCustomerAccountService;
	private OsanedB2BCustomerService osanedB2BCustomerService;

	@Override
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData)
			throws DuplicateUidException, CPIConnectionFailureException
	{
		if (!getUserFacade().isUserExisting(b2bCustomerData.getEmail()))
		{
			getOsanedB2BCustomerAccountService().registerB2BCustomer(b2bCustomerData);
		}
		else
		{
			throw new DuplicateUidException("Customer already exists : " + b2bCustomerData.getEmail());
		}

	}

	@Override
	public void setPriceRangeInB2BUnit(final String userId, final String priceRange)
	{

		osanedB2BCustomerAccountService.setPriceRangeInB2BUnit(userId, priceRange);
	}

	/**
	 * @return the osanedB2BCustomerAccountService
	 */
	public OsanedB2BCustomerAccountService getOsanedB2BCustomerAccountService()
	{
		return osanedB2BCustomerAccountService;
	}

	/**
	 * @param osanedB2BCustomerAccountService
	 *           the osanedB2BCustomerAccountService to set
	 */
	public void setOsanedB2BCustomerAccountService(final OsanedB2BCustomerAccountService osanedB2BCustomerAccountService)
	{
		this.osanedB2BCustomerAccountService = osanedB2BCustomerAccountService;
	}

	@Override
	public void activateCustomerProfile(final String userId, final String otp)
	{
		getOsanedB2BCustomerService().activateCustomerProfile(userId, otp);
	}

	/**
	 * @return the osanedB2BCustomerService
	 */
	public OsanedB2BCustomerService getOsanedB2BCustomerService()
	{
		return osanedB2BCustomerService;
	}

	/**
	 * @param osanedB2BCustomerService
	 *           the osanedB2BCustomerService to set
	 */
	public void setOsanedB2BCustomerService(final OsanedB2BCustomerService osanedB2BCustomerService)
	{
		this.osanedB2BCustomerService = osanedB2BCustomerService;
	}

	@Override
	public void acceptTermsAndCondition(final String userId)
	{
		getOsanedB2BCustomerService().acceptTermsAndCondition(userId);
	}

	@Override
	public void updateB2BCustomer(final B2BCustomerData b2bCustomerData)
	{
		getOsanedB2BCustomerService().updateB2BCustomer(b2bCustomerData);
	}




}
