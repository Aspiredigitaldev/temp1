/**
 *
 */
package de.hybris.osaned.facades.customer;

import de.hybris.platform.cmsfacades.dto.MediaFileDto;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceManagerOpenBalanceData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedData;
import de.hybris.platform.commercefacades.user.data.MaterialReceivedDataSet;
import de.hybris.platform.commercefacades.user.data.PaymentsData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderListData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author balamurugan
 *
 */
public interface OsanedFinanceB2BCustomerFacade
{
	/**
	 * @param b2bCustomerData
	 * @throws DuplicateUidException
	 */
	public void createFinanceCustomer(B2BCustomerData b2bCustomerData) throws DuplicateUidException;

	/**
	 * @param userId
	 * @param approverLevel1
	 * @param approverLevel2
	 * @param approverLevel3
	 */
	public void setAuthMatrixForUser(String userId, String approverLevel1, String approverLevel2, String approverLevel3);

	/**
	 * @param customerFinanceMasterSetupData
	 * @param userId
	 */
	public void createCustomerMasterData(CustomerFinanceMasterData customerFinanceMasterSetupData, String userId);

	/**
	 * @param userId
	 * @return CustomerFinanceMasterSetupData
	 */
	public CustomerFinanceMasterData getCustomerMasterData(String userId);

	/**
	 * @param FinanceManagerOpenBalanceData
	 * @param userId
	 */
	public void setFinanceOpenBalance(FinanceManagerOpenBalanceData financeManagerOpenBalanceData, String userId);

	/**
	 * @param purchaseOrderData
	 * @param media
	 */
	public void createPurchaseOrder(PurchaseOrderData purchaseOrderData, MediaFileDto media, String userId);

	/**
	 * @param userId
	 * @return PurchaseOrderListData
	 */
	public PurchaseOrderListData getPurchaseOrderData(String userId);

	/**
	 * @param materialReceivedData
	 */
	public void createMaterialReceivedData(MaterialReceivedData materialReceivedData, MediaFileDto media, String userId);


	/**
	 * @param userId
	 * @return MaterialReceivedDataSet
	 */
	public MaterialReceivedDataSet getapprovedSuppliers(String userId);

	/**
	 * @param userId
	 * @return MaterialReceivedDataSet
	 */
	public MaterialReceivedDataSet getapprovedPurchaseOrder(String userId, String supplierCode);

	/**
	 * @param paymentsData
	 * @param userId
	 */
	public void createPaymentsData(PaymentsData paymentsData, MediaFileDto media, String userId);
}
