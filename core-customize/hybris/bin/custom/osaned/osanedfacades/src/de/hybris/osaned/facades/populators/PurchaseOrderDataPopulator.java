/**
 *
 */
package de.hybris.osaned.facades.populators;

import de.hybris.osaned.core.model.PurchaseOrderModel;
import de.hybris.platform.commercefacades.user.data.PurchaseOrderData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.junit.Assert;


/**
 * @author balamurugan
 *
 */
public class PurchaseOrderDataPopulator implements Populator<PurchaseOrderModel, PurchaseOrderData>
{

	@Override
	public void populate(final PurchaseOrderModel source, final PurchaseOrderData target) throws ConversionException
	{
		Assert.assertNotNull("source cannot be null", source);
		Assert.assertNotNull("Target cannot be null", target);

		target.setPurchaseOrder(source.getCode());
		target.setSupplier(source.getSupplier().getBpCode());
		target.setItem(source.getItem().getItemCode());
		target.setItemDescription(source.getItem().getDescription());
		target.setPrice(String.valueOf(source.getPrice()));
		target.setQuantity(String.valueOf(source.getQuantity()));
		target.setAmount(String.valueOf(source.getAmount()));
		target.setCurrency(source.getCurrency().getIsocode());
		if (null != source.getItem() && null != source.getItem().getInventoryUnit())
		{
			target.setUnit(source.getItem().getInventoryUnit().getCode());
		}

	}

}
