/**
 *
 */
package de.hybris.osaned.facades.populators;

import de.hybris.osaned.core.model.FinanceCustomerMasterModel;
import de.hybris.platform.commercefacades.user.data.CustomerFinanceMasterData;
import de.hybris.platform.commercefacades.user.data.FinanceCompanyInfoData;
import de.hybris.platform.commercefacades.user.data.FinanceItemInfoData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.springframework.util.CollectionUtils;


/**
 * @author balamurugan
 *
 */
public class CustomerFinanceMasterSetupDataPopulator implements Populator<FinanceCustomerMasterModel, CustomerFinanceMasterData>
{

	@Override
	public void populate(final FinanceCustomerMasterModel source, final CustomerFinanceMasterData target)
			throws ConversionException
	{
		Assert.assertNotNull("source cannot be null", source);
		Assert.assertNotNull("Target cannot be null", target);

		if (null != source.getCustomerInfo())
		{
			final FinanceCompanyInfoData CustomerInfo = new FinanceCompanyInfoData();
			CustomerInfo.setBpCode(source.getCustomerInfo().getBpCode());
			CustomerInfo.setName(source.getCustomerInfo().getName());
			CustomerInfo.setAddress(source.getCustomerInfo().getAddress());
			CustomerInfo.setVatNumber(source.getCustomerInfo().getVatNumber());
			CustomerInfo.setCurrency(source.getCustomerInfo().getCurrency().getIsocode());
			CustomerInfo.setFinancialGroup(source.getCustomerInfo().getFinancialGroup());
			target.setCustomerInfo(CustomerInfo);
		}
		if (!CollectionUtils.isEmpty(source.getSupplierInfo()))
		{

			final List<FinanceCompanyInfoData> supplierInfoList = new ArrayList<>();
			source.getSupplierInfo().forEach(supplierInfoModel -> {
				final FinanceCompanyInfoData supplierInfo = new FinanceCompanyInfoData();
				supplierInfo.setBpCode(supplierInfoModel.getBpCode());
				supplierInfo.setName(supplierInfoModel.getName());
				supplierInfo.setAddress(supplierInfoModel.getAddress());
				supplierInfo.setVatNumber(supplierInfoModel.getVatNumber());
				supplierInfo.setCurrency(supplierInfoModel.getCurrency().getIsocode());
				supplierInfo.setFinancialGroup(supplierInfoModel.getFinancialGroup());
				supplierInfoList.add(supplierInfo);
			});
			target.setSupplierInfo(supplierInfoList);
		}
		if (!CollectionUtils.isEmpty(source.getItemInfo()))
		{
			final List<FinanceItemInfoData> itemInfoList = new ArrayList<>();
			source.getItemInfo().forEach(itemInfoModel -> {
				final FinanceItemInfoData financeItemInfoData = new FinanceItemInfoData();
				financeItemInfoData.setItemCode(itemInfoModel.getItemCode());
				financeItemInfoData.setDescription(itemInfoModel.getDescription());
				financeItemInfoData.setItemType(itemInfoModel.getType().getCode());
				financeItemInfoData.setItemGroup(itemInfoModel.getItemGroup());
				financeItemInfoData.setUnitSet(String.valueOf(itemInfoModel.getUnitSet()));
				financeItemInfoData.setInventoryUnit(itemInfoModel.getInventoryUnit().getCode());
				financeItemInfoData.setCurrency(itemInfoModel.getCurrency().getIsocode());
				itemInfoList.add(financeItemInfoData);
			});
			target.setItemInfo(itemInfoList);
		}


	}



}
