package de.hybris.osaned.facades.populators;



import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataList;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;




public class OsanedProductPopulator implements Populator<ProductModel, ProductData>
{
	@Resource
	private PriceDataFactory priceDataFactory;
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private CommerceCommonI18NService commerceCommonI18NService;

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		Assert.notNull(source, "ProductModel is null");
		target.setPicture(setpicture(source, target));
		target.setHrmsPriceData(getHRMSPriceData(source));
	}

	/**
	 * @param source
	 * @return
	 */
	private PriceDataList getHRMSPriceData(final ProductModel source)
	{
		final PriceDataList priceDataList = new PriceDataList();
		if (!CollectionUtils.isEmpty(source.getEurope1Prices()))
		{
			final List<PriceData> priceList = new ArrayList<PriceData>();
			final Collection<PriceRowModel> priceRows = source.getEurope1Prices();
			priceRows.forEach(pr -> {
				final PriceData priceData = new PriceData();
				if (null != pr.getCurrency())
				{
					priceData.setCurrencyIso(pr.getCurrency().getIsocode());
					priceData.setFormattedValue(getFormattedValue(pr));
				}
				priceData.setPriceType(PriceDataType.BUY);
				priceData.setValue(BigDecimal.valueOf(pr.getPrice()));
				if (null != pr.getUg())
				{
					final LanguageModel languageModel = commerceCommonI18NService.getDefaultLanguage();
					priceData.setPriceRange(enumerationService.getEnumerationName(pr.getUg(),
							commerceCommonI18NService.getLocaleForLanguage(languageModel)));
					priceData.setPriceRangeName(pr.getUg().getCode());

				}
				priceList.add(priceData);
			});
			priceDataList.setPriceDataList(priceList);
		}
		return priceDataList;
	}

	/**
	 * @return
	 */
	private String getFormattedValue(final PriceRowModel pr)
	{
		final String formattedValue = priceDataFactory
				.create(PriceDataType.BUY, BigDecimal.valueOf(pr.getPrice()), pr.getCurrency().getIsocode()).getFormattedValue();
		return formattedValue;
	}

	/**
	 * @param source
	 * @param target
	 * @return
	 */
	private MediaData setpicture(final ProductModel source, final ProductData target)
	{
		if (null != source.getPicture())
		{
			final MediaModel mediaModel = source.getPicture();
			final MediaData mediaData = new MediaData();
			mediaData.setCode(mediaModel.getCode());
			mediaData.setAltText(mediaModel.getAltText());
			mediaData.setUrl(mediaModel.getUrl());
			return mediaData;
		}
		return null;

	}

}
